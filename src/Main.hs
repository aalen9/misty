--  Copyright (c) 2019. Alen Arslanagic, Jorge A. Perez and Erik Voogd
--  and University of Groningen.
--  All rights reserved.
--
--  Redistribution and use in source and binary forms, with or without
--  modification, are permitted provided that the following conditions are met:
--
--  * Redistributions of source code must retain the above copyright notice,
--    this list of conditions and the following disclaimer.
--
--  * Redistributions in binary form must reproduce the above copyright notice,
--    this list of conditions and the following disclaimer in the documentation
--    and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,  THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES;  LOSS OF USE, DATA, OR PROFITS; # OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON  ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF  THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

module Main where

import Examples.SimpleExample as Ex0
import Examples.NamePassing as Ex1
import Examples.SelectBranching as Ex2
import Examples.RecursionEncoding as Ex3

import Misty

main = do
    putStrLn "Enter a number 0-3 to run the example:"
    ans <- getLine
    putStrLn "Enter a file path (otherwise ../output will be used):"
    fp <- getLine 
    --putStrLn $ show (length fp)  
    putStrLn "----------------------------"
    case ans of
      "0" -> mistyfp Ex0.prog fp 
      "1" -> mistyfp Ex1.prog fp 
      "2" -> mistyfp  Ex2.prog fp 
      "3" -> mistymufp Ex3.prog fp 
      _   -> putStrLn "Unknown"
    putStrLn "----------------------------"
