--  Copyright (c) 2019. Alen Arslanagic, Jorge A. Perez and Erik Voogd
--  and University of Groningen.
--  All rights reserved.
--
--  Redistribution and use in source and binary forms, with or without
--  modification, are permitted provided that the following conditions are met:
--
--  * Redistributions of source code must retain the above copyright notice,
--    this list of conditions and the following disclaimer.
--
--  * Redistributions in binary form must reproduce the above copyright notice,
--    this list of conditions and the following disclaimer in the documentation
--    and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,  THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES;  LOSS OF USE, DATA, OR PROFITS; # OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON  ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF  THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE GADTs #-}

module Misty.Semantics where

import Misty.Process
import Misty.Types

import Control.Monad.State as State
import Control.Monad.Identity as Identity

import qualified Data.List as List
import qualified Data.Map as Map
import qualified Data.Set as Set

import Misty.Channel

import Data.Char

-------------------------------------------------------------------------------
-- OPERATIONAL SEMANTICS
-------------------------------------------------------------------------------
data Reduced a where
  Reduced :: a -> Reduced a
  NonReduced :: a -> Reduced a


instance Functor Reduced where
  fmap g ra = case ra of
                Reduced a -> Reduced (g a)
                NonReduced a -> NonReduced (g a)

instance Applicative Reduced where
  (<*>) f a = let f' = reducedGetter f in
                case a of
                  Reduced a' -> Reduced (f' a')
                  NonReduced a' -> NonReduced (f' a')

  pure a = NonReduced a

instance Monad Reduced where
  (>>=) a f = case a of
                  Reduced b -> Reduced (reducedGetter $ f b)
                  NonReduced b -> f b
  return a = NonReduced a


reducedGetter :: Reduced a -> a
reducedGetter (Reduced b) = b
reducedGetter (NonReduced b) = b

substituteVar :: VarN -> Abstr -> Proc -> Proc
substituteVar x v p = applyAbs (SubstAbs $ Map.fromList [(x,v)]) p

-- Reduce Poly
freduceRecv :: ChannelN -> Abstr -> [Proc] -> Reduced [Proc]
freduceRecv u v [] = return $ []
freduceRecv u v procs =
    mapReduce f procs
      where
        f = \proc2 ->
                case proc2 of
                    Recv u' t x p' ->
                      if (u' == compl u) then
                        Reduced (applyAbs (SubstAbs $ Map.fromList [(x,v)]) p')
                      else
                        NonReduced proc2
                    _ -> NonReduced proc2


type ReduceSend a = StateT (Maybe Abstr) Reduced a

freduceSend' :: ChannelN -> [Proc] -> ReduceSend [Proc]
freduceSend' u [] = return $ []
freduceSend' u (x:xs) = case x of
                            Send u' v p' -> if (u' == compl u) then
                                              do
                                                put (Just v)
                                                (lift $ (Reduced (p':xs)))
                                            else
                                              do
                                                xs' <- freduceSend' u xs
                                                lift $ return $ (x:xs')

                            _ -> do
                                  xs' <- freduceSend' u xs
                                  lift $ return $ (x:xs')

reduceSend :: ChannelN -> VarN -> Proc -> [Proc] -> Reduced [Proc]
reduceSend u x p procs = do
                          (ps', jv) <- (runStateT (freduceSend' u procs) Nothing)
                          case jv of
                            Just v ->  Reduced (p':ps') where
                                        p' = substituteVar x v p
                            Nothing ->  NonReduced (ps')


reducePass :: [Proc]  -> Reduced [Proc]
reducePass [] = NonReduced []
reducePass (proc1:procs) =
    case proc1 of
        Send u v p    -> case (freduceRecv u v procs) of
                            Reduced procs' -> Reduced (p:procs')
                            NonReduced procs' -> do
                                                  procs'' <- reducePass procs
                                                  return (proc1:procs'')
        Recv u t x p  -> case reduceSend u x p procs of
                          Reduced p' -> Reduced p'
                          NonReduced p' -> do
                                                procs'' <- reducePass procs
                                                return (proc1:procs'')
        _             -> do
                          p1 <- return $ proc1
                          procs' <- reducePass procs
                          return $ (p1:procs')


-- Selection and Branch pass

type ReduceBranch a = StateT (Maybe Int) Reduced a

reduceBranch ::[Proc] -> Reduced [Proc]
reduceBranch [] = return $ []
reduceBranch (proc1:procs) =
  case proc1 of
      Select u l p -> case (freduceSel u l procs) of
                        Reduced procs' -> Reduced (p:procs')
                        NonReduced procs' -> NonReduced (proc1:procs')
      Branch u ps -> case (freduceBranch u ps procs) of
                        Reduced p' -> Reduced p'
                        NonReduced p' -> NonReduced (proc1:p')
      _           -> do
                      p1 <- return $ proc1
                      procs' <- reduceBranch procs
                      return $ (p1:procs')

freduceSel :: ChannelN -> Int -> [Proc] -> Reduced [Proc]
freduceSel u l [] = return $ []
freduceSel u l procs = mapReduce f procs where
                        f = \proc2 -> case proc2 of
                                        Branch u' ps -> if (compl u == u') then
                                                          Reduced (ps!!l)
                                                        else
                                                          NonReduced proc2
                                        _ -> NonReduced proc2


freduceBranch' :: ChannelN -> [Proc] -> ReduceBranch [Proc]
freduceBranch' u [] = return $ []
freduceBranch' u (p:ps) = case p of
                            Select u' l p' -> if (compl u == u') then
                                                do
                                                  put (Just l)
                                                  (lift $ (Reduced (p':ps)))
                                              else
                                                do
                                                  ps' <- freduceBranch' u ps
                                                  lift $ return (p:ps')
                            _ -> do
                                  ps' <- freduceBranch' u ps
                                  lift $ return (p:ps')

freduceBranch :: ChannelN -> [Proc] -> [Proc] -> Reduced [Proc]
freduceBranch u ps procs = do
                            (procs',jv) <- (runStateT (freduceBranch' u procs) Nothing)
                            case jv of
                              Just l -> Reduced (p':procs') where
                                          p' = ps!!l
                              Nothing -> NonReduced (procs')

reducedProc :: Reduced Proc -> Proc
reducedProc (Reduced p) = p
reducedProc (NonReduced p) = p



mapReduce :: (a -> Reduced a) -> [a] -> Reduced [a]
mapReduce f (x:xs) = case (f x) of
                      Reduced b -> Reduced (b:xs)
                      NonReduced b -> case (mapReduce f xs) of
                                        Reduced bs -> Reduced (b:bs)
                                        NonReduced bs -> NonReduced (b:bs)
mapReduce f [] = NonReduced []


-- main reduce function
reduce :: Proc -> Reduced Proc
reduce  (Comp pl) = let p0 = congruence0 pl in
  case p0 of
    [] -> NonReduced End
    (x:[]) -> reduce x
    (x:xs) -> let p = Comp (x:xs) in
                let p' = runIdentity $ evalStateT (scopeExtrude p) (Set.empty) in
                  case p' of
                    Comp pl' ->
                        case (reducePass pl') of
                          Reduced pl'' -> Reduced (Comp (congruence0 pl''))
                          NonReduced pl'' ->
                            case (reduceBranch pl'') of
                                Reduced pl3 -> Reduced (Comp (congruence0 pl3))
                                NonReduced pl3 ->
                                  case (mapReduce reduce pl3) of
                                      Reduced pl4 -> 
                                        Reduced (Comp (congruence0 pl4))
                                      NonReduced pl4 -> NonReduced (Comp pl4)
                    p'' -> reduce p''
reduce (App v u) = case v of
                      Abstr n t p -> Reduced (substituteName n u p)
                      _ -> NonReduced (App v u)

reduce (AppP v us) = case v of
                      AbstrP ns ts p -> let subs = SubstN $ Map.fromList (zip ns us) in
                                          Reduced (applyn subs p)
                      _ -> NonReduced (AppP v us)

reduce (Restr n t p) = do
                          p' <- reduce p
                          return $ (congruenceRestr $ Restr n t p')
reduce (BinOp op x y xy p) = 
    if (combineChannels op x y == xy) then
      return $ (BinOp op x y xy p)
      else
        case op of
          OpAdd -> let res = opCh (+) x y in
                      case res of
                        Just n -> Reduced $ substituteName xy n p
                        Nothing -> NonReduced $ BinOp op x y xy p
          OpSub -> let res = opCh (-) x y in
                      case res of
                        Just n -> Reduced $ substituteName xy n p
                        Nothing -> NonReduced $ BinOp op x y xy p
reduce p = NonReduced p

getIntSign :: String -> (Bool, String)
getIntSign n = if not (head n == '-') then
                    (True, n)
                    else
                      (False, tail n)

getIntFromString :: String -> Maybe Int
getIntFromString n = let (sign, n') = getIntSign n in
                      if sign then
                        case List.findIndex (not . isDigit) n of
                            Just r -> Just $ let (num1, rest) = splitAt r n in
                                          read num1 :: Int
                            Nothing -> Nothing
                        else
                          case List.findIndex (not . isDigit) n of
                            Just r -> Just $ let (num1, rest) = splitAt r n in
                                          - (read num1 :: Int)
                            Nothing -> Nothing

channelToInt :: ChannelN -> Maybe Int
channelToInt (Ch n) = getIntFromString n

intToChannel :: Int -> ChannelN
intToChannel n = Ch (show n ++ "(Int)")

opCh :: (Int -> Int -> Int) -> ChannelN -> ChannelN -> Maybe ChannelN
opCh op x y =
  do
    x' <- channelToInt x
    y' <- channelToInt y
    return $ intToChannel (op x' y')


-- Congruences
congruence0 :: [Proc] -> [Proc]
congruence0 pl = filter (\x -> x /= End) pl

congruenceRestr :: Proc -> Proc
congruenceRestr p@(Restr n t p') = if (Set.member n (ftvn p))
                                      then
                                        p
                                      else
                                        p'

congruenceRestr p = p

evalNameState :: NameState a -> a
evalNameState m = runIdentity $ evalStateT m (Set.empty)

scopeExtrude :: Proc -> NameState Proc
scopeExtrude p@(Comp pl) = let fv = channelToString (ftvn p) in
                            do
                              pl' <- mapNameState (scopeExtrude' fv) pl
                              set1 <- get
                              return $ foldr f (Comp $ flattenComp pl') set1 
                                      where
                                        f = \x -> \y -> Restr (fst x) (snd x) y

scopeExtrude p = return $ p

runScopeExtrude :: Proc -> Proc
runScopeExtrude p = runIdentity $ evalStateT (scopeExtrude p) Set.empty


flattenComp :: [Proc] -> [Proc]
flattenComp (p:ps) = case p of
                      Comp ps' -> (flattenComp ps') ++ (flattenComp ps)
                      _ -> (p:flattenComp ps)
flattenComp [] = []



channelToString :: Set.Set ChannelN -> Set.Set String
channelToString fvc = Set.map f fvc
                        where f = \x -> case x of
                                    Ch n -> n
                                    ChCmpl n -> n

channelToString' :: ChannelN -> String
channelToString' (Ch n) = n
channelToString' (ChCmpl n) = n


type NameState a = StateT (Set.Set (ChannelN, TSession)) Identity a

freshName :: String -> Int -> Set.Set String -> String
freshName oldname counter names =  if (Set.member (oldname ++ (show counter)) names) then
                                        freshName (oldname) (counter+1) names
                                      else
                                        (oldname ++ (show counter))

freshName' :: String -> Set.Set String -> String
freshName' name names = if (Set.member name names) then
                            freshName name 1 names
                          else
                            name


scopeExtrude' :: Set.Set String -> Proc -> NameState Proc
scopeExtrude' fns (Restr n t p) = let name = (freshName' (channelToString' n) fns) in
                                  let x = (case n of
                                              Ch n' -> Ch name
                                              ChCmpl n' -> ChCmpl name) in
                                    do
                                      set1 <- get
                                      put $ Set.insert (x,t) set1
                                      return $ substituteName n x p

scopeExtrude' _ p = return $ p



mapNameState :: (a -> NameState a) -> [a] -> NameState [a]
mapNameState f (p:ps) = do
                          p' <- f p
                          ps' <- mapNameState f ps
                          return $ (p':ps')
mapNameState f [] = return $ []


printReduced :: Show a => Reduced a -> String
printReduced (Reduced p) = "Reduced " ++ show p
printReduced (NonReduced p) = "NonReduced " ++ show p


-- OPERATIONAL SEMANTICS FOR POLYADIC HO

type ReducePolySend a = StateT (Maybe [PAbstr]) Reduced a
type NamePolyState a = StateT (Set.Set ([ChannelN], [MinM])) Identity a


freducePolyRecv :: ChannelN -> [PAbstr] -> [PProc] -> Reduced [PProc]
freducePolyRecv u vs procs = 
  mapReduce f procs
    where
      f = \proc2 -> 
        case proc2 of
            PRecv u' xs ts p' ->
              if (u' == compl u) then
                let l1 = zip xs vs in
                  Reduced (applyPolyAbs (SubstPolyAbs $ Map.fromList l1) p')
              else
                NonReduced proc2
            _ -> NonReduced proc2


ifvTest :: PSendee -> Bool
ifvTest = \x -> case x of
                  SAbstr _ -> True
                  SVar _ -> False

ifallv :: [PSendee] -> Bool
ifallv xs = all ifvTest xs

freducePolySend' :: ChannelN -> [PProc] -> ReducePolySend [PProc]
freducePolySend' u [] = return $ []
freducePolySend' u (x:xs) = 
    case x of
        PSend u' vs p' -> 
          let ifv = (ifallv vs) in
                            let abstrs = getAbstrfromSendee vs in
                              if ((u' == compl u) && ifv) then
                                  do
                                    put (Just abstrs)
                                    (lift $ (Reduced (p':xs)))
                                else
                                  do
                                    xs' <- freducePolySend' u xs
                                    lift $ return $ (x:xs')

        _ -> do
              xs' <- freducePolySend' u xs
              lift $ return $ (x:xs')


reducePolySend :: ChannelN -> [VarN] -> PProc -> [PProc] -> Reduced [PProc]
reducePolySend u xs p procs = 
    do
        (ps', jv) <- (runStateT (freducePolySend' u procs) Nothing)
        case jv of
          Just vs ->  return $ (p':ps') where
                      p' = applyPolyAbs (SubstPolyAbs $ Map.fromList (zip xs vs)) p
          Nothing ->  return $ (procs)



scopeExtrudePoly' :: Set.Set String -> PProc -> NamePolyState PProc
scopeExtrudePoly' fns (PRestr nts p) = 
  let names = map (\n->(freshName' (channelToString' n) fns)) ns  in
      let xs = map (\(n,name)->(case n of
              Ch n' -> Ch name
              ChCmpl n' -> ChCmpl name)) (zip ns names) in
        let subst = (zip ns xs) ++ (zip (map compl ns) (map compl xs)) in
          do
            set1 <- get
            put $ Set.insert (xs,map snd nts) set1
            return $ applyn (SubstN $ Map.fromList subst) p
          where
            ns = map fst nts
scopeExtrudePoly' _ p = return $ p


mapNamePolyState :: (a -> NamePolyState a) -> [a] -> NamePolyState [a]
mapNamePolyState f (p:ps) = do
                          p' <- f p
                          ps' <- mapNamePolyState f ps
                          return $ (p':ps')
mapNamePolyState f [] = return $ []



flattenPolyComp :: [PProc] -> [PProc]
flattenPolyComp (p:ps) = 
  case p of
      PComp ps' -> (flattenPolyComp ps') ++ (flattenPolyComp ps)
      _  -> (p:flattenPolyComp ps)
flattenPolyComp [] = []

getAbstrfromSendee :: [PSendee] -> [PAbstr]
getAbstrfromSendee xs = let (as) = (filter ifvTest xs) in
                            map (\x -> case x of
                                        SAbstr v -> v
                                        SVar x -> PAbstr [] [] PEnd) as


reducePassPoly :: [PProc] -> Reduced [PProc]
reducePassPoly (proc1:procs) =
  case proc1 of
      PSend u vs p  -> 
        if (ifallv vs) then
            let abstrs = getAbstrfromSendee vs in
              case (freducePolyRecv u abstrs procs) of
                Reduced procs' -> Reduced (p:procs')
                NonReduced procs' -> case (reducePassPoly procs) of
                                      Reduced procs'' -> Reduced (proc1:procs'')
                                      NonReduced procs'' -> NonReduced (proc1:procs'')
            else
              case (reducePassPoly procs) of
                                    Reduced procs'' -> Reduced (proc1:procs'')
                                    NonReduced procs'' -> NonReduced (proc1:procs'')
      PRecv u xs ts p  -> case reducePolySend u xs p procs of
                            Reduced p' -> Reduced p'
                            NonReduced p' -> do
                                              p'' <- reducePassPoly p'
                                              return (proc1:p'')
      _             -> case (reducePassPoly procs) of
                            Reduced procs'' -> Reduced (proc1:procs'')
                            NonReduced procs'' -> NonReduced (proc1:procs'')

reducePassPoly [] = return $ []


reducePoly :: PProc -> Reduced PProc
reducePoly p@(PComp pl) =
  let p0 = congruencePoly0 pl in
    case p0 of
      [] -> NonReduced PEnd
      (x:[]) -> reducePoly x
      (x:xs) -> 
        let p' = runIdentity $ evalStateT (scopeExtrudePoly (PComp p0)) (Set.empty) in
              case p' of
                PComp pl' ->
                    case (reducePassPoly pl') of
                      Reduced p'' -> Reduced (PComp (congruencePoly0 p''))
                      NonReduced p'' ->
                        case (reducePolyBranch p'') of
                            Reduced pl3 -> Reduced (PComp (congruencePoly0 pl3))
                            NonReduced pl3 ->
                                case (mapReduce reducePoly pl3) of
                                    Reduced pl4 -> 
                                      Reduced (PComp (congruencePoly0 pl4))
                                    NonReduced pl4 -> NonReduced (PComp pl4)
                p'' -> reducePoly p''
reducePoly (PApp v us) = 
    case v of
      PAbstr ns ts p -> Reduced (applyn (SubstN $ Map.fromList (zip ns us)) p)
reducePoly (PRestr nts p) = do
                             p' <- reducePoly p
                             return $ (congruencePolyRestr $ PRestr nts p')
reducePoly (PBinOp op x y xy p) = 
    if (combineChannels op x y == xy) then
        return $ (PBinOp op x y xy p)
        else
          case op of
            OpAdd -> 
              let res = opCh (+) x y in
                  case res of
                    Just n -> Reduced $ applyn (SubstN $ Map.fromList [(xy,n)]) p
                    Nothing -> NonReduced $ PBinOp op x y xy p
            OpSub -> 
              let res = opCh (-) x y in
                  case res of
                    Just n -> Reduced $ applyn (SubstN $ Map.fromList [(xy,n)]) p
                    Nothing -> NonReduced $ PBinOp op x y xy p
reducePoly p = return p



reducePolyBranch :: [PProc] -> Reduced [PProc]
reducePolyBranch [] = return $ []
reducePolyBranch (proc1:procs) = case proc1 of
                    PSelect u l p -> case (freducePolySel u l procs) of
                                      Reduced procs' -> Reduced (p:procs')
                                      NonReduced procs' -> NonReduced (proc1:procs')
                    PBranch u ps -> case (freducePolyBranch u ps procs) of
                                      Reduced p' -> Reduced p'
                                      NonReduced p' -> NonReduced (proc1:p')
                    _           -> do
                                    p1 <- return $ proc1
                                    procs' <- reducePolyBranch procs
                                    return $ (p1:procs')


freducePolySel :: ChannelN -> Int -> [PProc] -> Reduced [PProc]
freducePolySel u l [] = return $ []
freducePolySel u l procs = mapReduce f procs where
                        f = \proc2 -> case proc2 of
                                        PBranch u' ps -> if ((compl u) == u') then
                                                          Reduced (ps!!l)
                                                        else
                                                          NonReduced proc2
                                        _ -> NonReduced proc2

freducePolyBranch' :: ChannelN -> [PProc] -> ReduceBranch [PProc]
freducePolyBranch' u [] = return $ []
freducePolyBranch' u (p:ps) = case p of
                            PSelect u' l p' -> if ((compl u) == u') then
                                                do
                                                  put (Just l)
                                                  (lift $ (Reduced (p':ps)))
                                              else
                                                do
                                                  ps' <- freducePolyBranch' u ps
                                                  lift $ return (p:ps')
                            _ -> do
                                  ps' <- freducePolyBranch' u ps
                                  lift $ return (p:ps')

freducePolyBranch :: ChannelN -> [PProc] -> [PProc] -> Reduced [PProc]
freducePolyBranch u ps procs = do
                (procs',jv) <- (runStateT (freducePolyBranch' u procs) Nothing)
                case jv of
                  Just l -> Reduced (p':procs') where
                              p' = ps!!l
                  Nothing -> NonReduced (procs')

-- Congruences
scopeExtrudePoly :: PProc -> NamePolyState PProc
scopeExtrudePoly p@(PComp pl) = 
  let fv = channelToString (ftvn p) in
      do
        pl' <- mapNamePolyState (scopeExtrudePoly' fv) pl
        set1 <- get
        return $ foldr f (PComp $ flattenPolyComp pl') set1 where
          f = \x -> \y -> PRestr (zip (fst x) (snd x)) y

scopeExtrudePoly p = return $ p

congruencePoly0 :: [PProc] -> [PProc]
congruencePoly0 pl = filter (\x -> x /= PEnd) pl

congruencePolyRestr :: PProc -> PProc
congruencePolyRestr (PRestr nts p) = 
    if (Set.size (Set.intersection  (Set.fromList (map fst nts)) (ftvn p)) == 0)
       then
          p
       else
         let intr = Set.intersection (Set.fromList (map fst nts)) (ftvn p) in
           let diff = Set.difference (Set.fromList (map fst nts)) intr in
             PRestr (filter (\(n,t) -> Set.member n intr) nts) p
congruencePolyRestr p = p


reduceStar :: Reduced Proc -> Reduced Proc
reduceStar reduct = reduce $ reducedGetter reduct


reducePolyStar :: Reduced PProc -> Reduced PProc
reducePolyStar reduct = reducePoly $ reducedGetter reduct


reducePolys' :: Reduced PProc -> Int -> [Reduced PProc]
reducePolys' p 0 = []
reducePolys' p n = let pR = reducePolyStar p in
                    (pR:(reducePolys' pR (n-1)))

reducePolys :: Reduced PProc -> Int -> [Reduced PProc]
reducePolys p n = (p:(reducePolys' p n))

reduceFull' :: Proc -> [Proc]
reduceFull' p = let reduct = NonReduced p in
                  let pr = reduceStar reduct in
                    case pr of
                      Reduced pr' -> (pr':(reduceFull' pr'))
                      NonReduced pr' -> []

reduceFull :: Proc -> [Proc]
reduceFull p = (p:(reduceFull' p))

reducePFull' :: PProc -> [PProc]
reducePFull' p = let reduct = NonReduced p in
                  let pr = reducePolyStar reduct in
                    case pr of
                      Reduced pr' -> (pr':(reducePFull' pr'))
                      NonReduced pr' -> []

reducePFull :: PProc -> [PProc]
reducePFull p = (p:reducePFull' p)
