--  Copyright (c) 2019. Alen Arslanagic, Jorge A. Perez and Erik Voogd
--  and University of Groningen.
--  All rights reserved.
--
--  Redistribution and use in source and binary forms, with or without
--  modification, are permitted provided that the following conditions are met:
--
--  * Redistributions of source code must retain the above copyright notice,
--    this list of conditions and the following disclaimer.
--
--  * Redistributions in binary form must reproduce the above copyright notice,
--    this list of conditions and the following disclaimer in the documentation
--    and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,  THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES;  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON  ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF  THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE GADTs #-}

module Misty.Channel where

import Data.Char
import Data.List as List

data ChannelN = Ch String | ChCmpl String
   deriving (Eq, Ord)
type VarN = String
type SelN = String

compl :: ChannelN -> ChannelN
compl (Ch n) = ChCmpl n
compl (ChCmpl n) = Ch n

getkName' :: String -> String
getkName' (n:ns) = if (isDigit n) then
                    (getkName' ns) ++ [n]
                   else
                     []
getkName' [] = []

getuName' :: String -> String -> String
getuName' num str = drop (length num) str

getkName :: String -> (Int, String)
getkName ns = let x = getkName' $ reverse ns in
                case x of
                  [] -> (-1, ns)
                  _ -> (read x :: Int, reverse $ getuName' x (reverse ns))

printName :: String -> String
printName n = let revn = reverse n in
                let r = List.findIndex (not . isDigit) revn in
                  let r' = case r of
                        Just r'' -> r''
                        Nothing -> length revn in
                  let (k, n') = splitAt r' revn in
                    reverse n' ++ "_{" ++ reverse k ++ "}"

printChannel :: ChannelN -> String
printChannel (Ch n) = "Ch " ++ n
prrintChannel (ChCmpl n) = "ChCmpl " ++ n


getkChannel :: ChannelN -> (Int, String)
getkChannel (Ch n) = getkName n
getkChannel (ChCmpl n) = getkName n

getBindChannelName :: ChannelN -> ChannelN
getBindChannelName (Ch n) = let (k, m) = getkChannel (Ch n) in
                              (Ch m)
getBindChannelName (ChCmpl n) = let (k, m) = getkChannel (ChCmpl n) in
                                  (ChCmpl m)

xtuple :: ChannelN -> Int -> [ChannelN]
xtuple ch n = case ch of
                Ch n' -> map (\x -> Ch (n' ++ show x)) [1..n]
                ChCmpl n' -> map (\x -> ChCmpl (n' ++ show x)) [1..n]

utuple :: Int -> [ChannelN]
utuple n = map (\x -> Ch ("u" ++ show x)) [1..n]

utupleto :: Int -> ChannelN -> [ChannelN]
utupleto upto (Ch ui) = let (i,u) = getkName ui in
                          if (i > -1) then
                            map (\x -> Ch (u ++ show x)) [i..upto]
                          else
                            map (\x -> Ch (u ++ show x)) [1..upto]
utupleto upto (ChCmpl ui) = let (i,u) = getkName ui in
                              if (i > -1) then
                                map (\x -> ChCmpl (u ++ show x)) [i..upto]
                              else
                                map (\x -> ChCmpl (u ++ show x)) [1..upto]


utupletosub :: Int -> String -> ChannelN -> [ChannelN]
utupletosub upto sup (Ch ui) =
    let n = (Ch (ui ++ "^{" ++ sup ++ "}")) in
      xtuple n upto
utupletosub upto sup (ChCmpl ui) =
  let n = (ChCmpl (ui ++ "^{" ++ sup ++ "}")) in
    xtuple n upto

getc :: Int -> Bool -> ChannelN
getc k b = case b of
            True -> Ch ("c" ++ show k)
            False -> ChCmpl ("c" ++ show k)


succName :: ChannelN -> ChannelN
succName n = case n of
              Ch n' -> let (k,u) = getkName n' in
                          Ch (u ++ show (succ k))
              ChCmpl n' -> let (k,u) = getkName n' in
                          ChCmpl (u ++ show (succ k))
