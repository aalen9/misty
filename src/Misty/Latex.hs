--  Copyright (c) 2019. Alen Arslanagic, Jorge A. Perez and Erik Voogd
--  and University of Groningen.
--  All rights reserved.
--
--  Redistribution and use in source and binary forms, with or without
--  modification, are permitted provided that the following conditions are met:
--
--  * Redistributions of source code must retain the above copyright notice,
--    this list of conditions and the following disclaimer.
--
--  * Redistributions in binary form must reproduce the above copyright notice,
--    this list of conditions and the following disclaimer in the documentation
--    and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,  THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES;  LOSS OF USE, DATA, OR PROFITS; # OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON  ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF  THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE GADTs #-}

module Misty.Latex where

import Misty.Process
import Misty.Types
import Misty.Channel


import qualified Data.List as List
import qualified Data.Map as Map
import qualified Data.Set as Set

--------------------------------------------------------------------------------
-- Latex
--------------------------------------------------------------------------------

class Latex a where
  latex :: a -> String
  latexSuppresTypes :: a -> String


printVar :: String -> String
printVar n = let (k,u)= getkName n in
                if (k > -1) then
                  u ++ "_{" ++ show k ++ "}"
                else
                  u


instance Show ChannelN where
  show (Ch n) = n
  show (ChCmpl n) = "^" ++ n


instance Latex ChannelN where
  latex (Ch n) = printName n
  latex (ChCmpl n) = "\\bar{" ++ printName n ++ "}"
  latexSuppresTypes x = latex x


latexS :: Latex a => [a] -> String
latexS xs =
  ((reverse . (drop 1) . reverse) (foldr (\x-> \y-> latex x ++ "," ++ y) "" xs))

showS :: Show a => [a] -> String
showS xs =
  ((reverse . (drop 1) . reverse) (foldr (\x-> \y-> show x ++ "," ++ y) "" xs))

latexSuppresTypesS :: Latex a => [a] -> String
latexSuppresTypesS xs =
  ((reverse . (drop 1) . reverse)
    (foldr (\x-> \y-> latexSuppresTypes x ++ "," ++ y) "" xs))


instance Show Abstr where
  show (Abstr x t p) = "lam " ++ show x ++ ":" ++ show t ++ "." ++ show p
  show (AbstrP xs ts p) = "lam " ++ showS xs ++ ":" ++ showS ts ++ "." ++ show p

instance Latex Abstr where
  latex (Abstr x t p) =
    "\\lambda " ++ latex x ++ ":" ++ colorType (latex t) ++ "."  ++ latex p
  latex (AbstrP xs ts p) =
    "\\lambda " ++ latexS xs ++ ":" ++ colorType (latexS ts) ++ "."  ++ latex p
  latexSuppresTypes (Abstr x t p) =
    "\\lambda " ++ latexSuppresTypes x ++ "."  ++ latexSuppresTypes p
  latexSuppresTypes (AbstrP xs ts p) =
    "\\lambda  " ++ latexSuppresTypesS xs ++ "."  ++ latexSuppresTypes p

instance Show Proc where
  show (Send u v p) = show u ++ "!" ++ "<" ++ show v ++ ">."
                                    ++ show p
  show (SendVar u y p) = show u ++ "!" ++ "<" ++ y ++ ">." ++ show p
  show (Recv u t x p) = show u ++ "?" ++ "(" ++ x ++ ":" ++ show t ++ ")."
                                    ++ show p
  show (App v u) = show v ++ " " ++ show u
  show (AppP v us) = show v ++ " " ++ showS us
  show (AppVar x u) = x ++ " " ++ show u
  show (AppVarP x us) = x ++ " " ++ showS us
  show (Comp p) = drop 3 xs where
                  xs = (foldr ((++) . (++) (" \\ | \\ ") . show) "" p)
  show (Restr n t p) = "(nu " ++ show n ++ ":" ++ show t ++ ")" ++ "(" ++ show p ++ ")"
  show (Select u l p) = show u ++ "<|" ++ "l" ++ show l ++ "." ++ show p
  show (Branch u ps) =
    show u ++ "|>" ++ "{" ++
    (drop 3 (foldr ((++) . (++) (" \\ | \\ ") . show) "" ps)) ++ "}"
  show End = "0"

instance Latex Op where
  latex (OpAdd) = "+"
  latex (OpSub) = "-"
  latexSuppresTypes p = latex p

instance Latex Proc where
  latex (Send u v p) =
    latex u ++ "!" ++ " \\langle " ++ latex v ++ " \\rangle."
                          ++ latex p
  latex (SendVar u y p) =
    latex u ++ "!" ++ " \\langle " ++ printVar y ++ " \\rangle."
                                  ++ latex p
  latex (Recv u t x p) =
    latex u ++ "?" ++ "(" ++ printVar x ++ ":" ++ colorType (latex t) ++ ")."
                                    ++ latex p
  latex (App v u) = latex v ++ " \\ " ++ latex u
  latex (AppP v us) = latex v ++ " \\ " ++ latexS us
  latex (AppVar x u) = printVar x ++ " \\ " ++ latex u
  latex (AppVarP x us) = printVar x ++ " \\ " ++ latexS us
  latex (Comp p) = drop 8 xs where
                  xs = (foldr ((++) . (++) (" \\ | \\\\ ") . latex) "" p)
  latex (Restr n t p) =
      "(\\nu " ++ latex n ++ ":" ++ colorType (latex t) ++ ")" ++ "(" ++ latex p ++ ")"
  latex End = "0 "
  latex (Select u l p) =
      latex u ++ "<" ++ "l" ++ "_" ++ "{" ++ show l ++ "}" ++ "." ++ latex p
  latex (Branch u ps) =
      latex u ++ ">" ++ "\\{" ++
              (drop 3 (foldr ((++) . (++) (" \\ | \\ ") . latex) "" ps)) ++"\\}"
  latex (BinOp op x y xy p) =
      latex x ++ " " ++ latex op ++ " " ++ latex y ++ "\\to" ++ latex xy ++ "." ++ latex p
  latexSuppresTypes (Send u v p) =
    latexSuppresTypes u ++ "!" ++ " \\langle  " ++ latexSuppresTypes v ++ " \\rangle."
                                    ++ latexSuppresTypes p
  latexSuppresTypes (SendVar u y p) =
    latexSuppresTypes u ++ "!" ++ " \\langle  " ++ printVar y ++ " \\rangle."
                                    ++ latexSuppresTypes p
  latexSuppresTypes (Recv u t x p) =
    latexSuppresTypes u ++ "?" ++ "(" ++ printVar x ++ ")."
                                    ++ latexSuppresTypes p
  latexSuppresTypes (App v u) =
    latexSuppresTypes v ++ " \\ " ++ latexSuppresTypes u
  latexSuppresTypes (AppP v us) =
    latexSuppresTypes v ++ " \\ " ++ latexSuppresTypesS us
  latexSuppresTypes (AppVar x u) =
    printVar x ++ " \\ " ++ latexSuppresTypes u
  latexSuppresTypes (AppVarP x us) =
    printVar x ++ " \\ " ++ latexSuppresTypesS us
  latexSuppresTypes (Comp p) = drop 8 xs where
                  xs = (foldr ((++) . (++) (" \\ | \\\\ ") . latexSuppresTypes) "" p)
  latexSuppresTypes (Select u l p) =
    latex u ++ "<" ++ "l" ++ "_" ++ "{" ++ show l ++ "}" ++ "." ++ latexSuppresTypes p
  latexSuppresTypes (Branch u ps) =
    latex u ++ ">" ++ "\\{" ++
      (drop 3 (foldr ((++) . (++) (" \\ | \\ ") . latexSuppresTypes) "" ps)) ++"\\}"
  latexSuppresTypes (Restr n t p) =
      "(\\nu " ++ latexSuppresTypes n ++ ")" ++ "(" ++ latexSuppresTypes p ++ ") "
  latexSuppresTypes End = "0 "
  latexSuppresTypes (BinOp op x y xy p) =
    latex x ++ " " ++ latexSuppresTypes op ++
      " " ++ latex y ++ " \\to " ++ latex xy ++ "." ++ latexSuppresTypes p


instance Show TAbs where
  show (TAbs c) = show c ++ "-->*"

instance Latex TAbs where
  latex (TAbs c) = latex c ++ "\\to \\diamond "
  latex (TAbsP cs) = latexS cs ++ "\\to \\diamond "
  latexSuppresTypes (TAbs c) = latexSuppresTypes c ++ "\\to \\diamond "
  latexSuppresTypes (TAbsP cs) = ""

instance Show TSession where
  show (TSend u s) = "!<" ++ show u ++ ">;" ++ show s
  show (TRecv u s) = "?(" ++ show u ++ ");" ++ show s
  show (TBool) = "bool"
  show (TEnd) = "end"
  show (TSl ts) = "sel"
  show (TBra ts) = "bra"
  show (TMu t ts) = "mu"
  show (TMuVar t) = "mu t"


instance Latex MuVar where
  latex (MuVar t) = t
  latexSuppresTypes t = latex t

colorType :: String -> String
colorType x = "\\color{blue}" ++ x ++ "\\color{black}"

instance Latex TSession where
  latex (TSend u s) = "! \\langle " ++ latex u ++ "\\rangle ;" ++ latex s
  latex (TRecv u s) = "?(" ++ latex u ++ ");" ++ latex s
  latex (TSl ts) = "sel"
  latex (TBra ts) = "bra"
  latex (TBool) = "bool "
  latex (TEnd) = "end "
  latex (TMu t ts) = "\\mu" ++ " " ++ "\\"++ " " ++ latex t ++ "."++ latex ts
  latex (TMuVar t) =  latex t
  latexSuppresTypes x =  latex x


instance Show MuVar where
  show (MuVar t) = show t

channelToString' :: ChannelN -> String
channelToString' (Ch n) = n
channelToString' (ChCmpl n) = n

instance Show PAbstr where
  show (PAbstr x t p) = "lam " ++ show x ++":" ++ show t ++ "." ++ show p

instance Show PSendee where
  show (SAbstr v) = show v
  show (SVar x) = show x

instance Latex PAbstr where
  latex (PAbstr x t p) =
    "\\lambda " ++
      ((reverse . (drop 1) . reverse)
        (foldr (\x-> \y-> latex x ++ "," ++ y) "" x)) ++ ":" ++
          ((reverse . (drop 1) . reverse)
            (foldr (\x-> \y-> latexSuppresTypes x ++ "," ++ y) "" t)) ++ "." ++ latex p
  latexSuppresTypes (PAbstr x t p) =
    " \\lambda " ++ "[" ++
      ((reverse . (drop 1) . reverse)
        (foldr (\x-> \y-> latexSuppresTypes x ++ "," ++ y) "" x)) ++
          "]" ++ "." ++ latexSuppresTypes p



instance Latex PSendee where
  latex (SAbstr v) = latex v
  latex (SVar x) = printVar x
  latexSuppresTypes (SAbstr v) = latexSuppresTypes v
  latexSuppresTypes (SVar x) = printVar x



instance Show PProc where
  show (PSend u v p) = show u ++ "!" ++ "<" ++ show v ++ ">."
                                    ++ show p
  show (PRecv u x t p) = show u ++ "?" ++ "(" ++ show x ++ ":" ++ show t ++ ")."
                                    ++ show p
  show (PApp v u) = show v ++ " " ++ show u
  show (PAppVar x u) = x ++ " " ++ show u
  show (PComp pl) = xs where
                    (x:xs) = (foldr ((++) . (++) (" | ") . show) "" pl)
  show (PRestr nts p) =
    "(nu " ++ (foldr printChannelTypes "" nts) ++ ")" ++ "(" ++ show p ++ ")"
  show (PSelect u l p) =
    show u ++ "<|" ++ "l" ++ show l ++ "." ++ show p
  show (PBranch u ps) =
    show u ++ "|>" ++ "{" ++ (drop 3 (foldr ((++) . (++) (" \\ | \\ ") . show) "" ps)) ++ "}"
  show PEnd = "0"



printChannelTypes :: (ChannelN, MinM) -> String -> String
printChannelTypes (n,t) s = show n ++ ":" ++ show t ++ "," ++ s


instance Latex PProc where
  latex (PSend u v p) =
    latex u ++ "!" ++ " \\langle " ++ ((reverse . (drop 1) . reverse)
      (foldr (\x -> \y ->latex x ++ "," ++ y) "" v)) ++ " \\rangle " ++ ";"
                                    ++ latex p
  latex (PRecv u x t p) =
    latex u ++ "?" ++ "(" ++ show x ++ ":" ++ ((reverse . (drop 1) . reverse)
      (foldr (\x -> \y ->latex x ++ "," ++ y) "" t)) ++ ")."
                                    ++ latex p
  latex (PApp v u) =
    latex v ++ " \\ " ++ ((reverse . (drop 1) . reverse)
      (foldr (\x -> \y ->latex x ++ "," ++ y) "" u))
  latex (PAppVar x u) =
    printVar x ++ " \\ " ++
      ((reverse . (drop 1) . reverse) (foldr (\x -> \y ->latex x ++ "," ++ y) "" u))
  latex (PComp pl) = xs where
                      (x:xs) = (foldr ((++) . (++) (" \\ | \\\\ ") . latex) "" pl)
  latex (PRestr nts p) =
    "\\nu " ++ "[" ++
      (foldr (pairToString latex latex) "" nts) ++ "]" ++ ")" ++ "(" ++ latex p ++ ")"
  latex PEnd = "0 "
  latex (PSelect u l p) =
    latex u ++ "<" ++ "l" ++ "_" ++ "{" ++ show l ++ "}" ++ "." ++ latex p
  latex (PBranch u ps) =
    latex u ++ ">" ++ "\\{" ++
      (drop 3 (foldr ((++) . (++) (" \\ | \\ ") . latex) "" ps)) ++"\\}"
  latex (PBinOp op x y xy p) =
    latex x ++ " " ++ latex op ++ " " ++ latex y ++ " \\to " ++ latex xy ++ "." ++ latex p

  latexSuppresTypes (PSend u v p) =
    let str1 = if (length v == 0) then
                  ""
                else
                  ((reverse . (drop 1) . reverse)
                    (foldr (\x-> \y-> latexSuppresTypes x ++ "," ++ y) "" v))
        in
          latexSuppresTypes u ++ "!" ++
            " \\langle " ++ str1 ++ " \\rangle " ++ "." ++ latexSuppresTypes p

  latexSuppresTypes (PRecv u x t p) =
    let str1 = if (length x == 0) then
                  ""
                else
                  ((reverse . (drop 1) . reverse)
                    (foldr (\x-> \y-> printVar x ++ "," ++ printVar y) "" x))
        in
          latexSuppresTypes u ++ "?" ++ "(" ++ str1 ++ ")." ++ latexSuppresTypes p
  latexSuppresTypes (PApp v u) =
    latexSuppresTypes v ++ " \\ " ++ "(" ++ ((reverse . (drop 1) . reverse)
      (foldr (\x-> \y-> latexSuppresTypes x ++ "," ++ y) "" u)) ++ ")"
  latexSuppresTypes (PAppVar x u) =
    printVar x ++ " \\ " ++ "(" ++ ((reverse . (drop 1) . reverse)
      (foldr (\x-> \y-> latexSuppresTypes x ++ "," ++ y) "" u)) ++ ")"
  latexSuppresTypes (PComp pl) =
    "(" ++ drop 8 xs ++ ")"
      where
        xs = (foldr ((++) . (++) (" \\ | \\\\ ") . latexSuppresTypes) "" pl)
  latexSuppresTypes (PRestr nts p) =
    if (length nts > 5) then
          " (" ++ "\\nu "  ++  (latexSuppresTypesS (map fst nts)) ++ ")" ++
            " \\\\ " ++ "(" ++ latexSuppresTypes p ++ ")"
      else
          " (" ++ "\\nu "  ++  (latexSuppresTypesS (map fst nts)) ++ ")" ++
            "(" ++ latexSuppresTypes p ++ ")"
  latexSuppresTypes PEnd = "0 "
  latexSuppresTypes (PSelect u l p) =
    latexSuppresTypes u ++ "<" ++ "l" ++ "_" ++ "{" ++ show l ++ "}" ++
      "." ++ latexSuppresTypes p
  latexSuppresTypes (PBranch u ps) =
    latexSuppresTypes u ++ ">" ++ "\\{" ++
      (drop 3 (foldr ((++) . (++) (" \\ | \\ ") . latexSuppresTypes) "" ps)) ++"\\}"
  latexSuppresTypes (PBinOp op x y xy p) =
    latex x ++ " " ++ latexSuppresTypes op ++ " " ++ latex y ++ " \\to " ++
      latex xy ++ "." ++ latexSuppresTypes p


pairToString :: (ChannelN -> String) -> (MinM -> String ) ->
                  (ChannelN, MinM) -> String -> String
pairToString f1 f2 (c,t) s = (f1 c) ++ ":" ++  (f2 t) ++ "," ++ s


instance Show PTAbs where
  show (PTAbs c) = show c ++ "-->*"

instance Latex PTAbs where
  latex (PTAbs c) =
    ((reverse . (drop 1) . reverse) (foldr (\x -> \y ->latex x ++ "," ++ y) "" c))
    ++ "\\to \\diamond "
  latexSuppresTypes x = latex x

instance Show PTSession where
  show (PTSend u s) = "!<" ++ show u ++ ">;" ++ show s
  show (PTRecv u s) = "?(" ++ show u ++ ");" ++ show s
  show (PTSl ts) = "sel"
  show (PTBra ts) = "bra"
  show (PTBool) = "bool "
  show (PTEnd) = "end"

instance Show MinM where
  show (MinSend m) = "!<" ++ show m ++ ">;end"
  show (MinRecv m) = "?(" ++ show m ++ ");end"
  show (MinSh m) = "<" ++ show m ++ ">"
  show (MinBool) = "mbool"
  show (MinSel ts) = "sel"
  show (MinBra ts) = "bra"
  show (MinEnd) = "end"

instance Show MinU where
  show (MinULin ts) =
    ((reverse . (drop 1) . reverse)
      (foldr (\x -> \y ->latex x ++ "," ++ y) "" ts))  ++ "\\multimap \\diamond "
  show (MinUSh ts) =
    ((reverse . (drop 1) . reverse) (foldr (\x -> \y ->latex x ++ "," ++ y) "" ts))
      ++ "\\to \\diamond "

instance Latex PTSession where
  latex (PTSend u s) =
    "!\\langle " ++ ((reverse . (drop 1) . reverse)
      (foldr (\x -> \y -> latex x ++ "," ++ y) "" u)) ++ "\\rangle;" ++ latex s
  latex (PTRecv u s) =
    "?(" ++ ((reverse . (drop 1) . reverse)
      (foldr (\x -> \y -> latex x ++ "," ++ y) "" u)) ++ ");" ++ latex s
  latex (PTBool) = "bool "
  latex (PTEnd) = "end "
  latex (PTSl ts) = "sel"
  latex (PTBra ts) = "bra"
  latexSuppresTypes x = latex x

instance Latex MinM where
  latex (MinSend m) =
    "!\\langle " ++ ((reverse . (drop 1) . reverse)
      (foldr (\x -> \y -> latex x ++ "," ++ y) "" m)) ++ "\\rangle;end"
  latex (MinRecv m) =
    "?(" ++ ((reverse . (drop 1) . reverse)
      (foldr (\x -> \y -> latex x ++ "," ++ y) "" m)) ++ ");end"
  latex (MinBool) = "bool "
  latex (MinEnd) = "end "
  latex (MinSh m) =
    "\\langle" ++ ((reverse . (drop 1) . reverse)
      (foldr (\x -> \y -> latex x ++ "," ++ y) "" m)) ++ "\\rangle"
  latex (MinSel ts) =
    "\\oplus " ++ "\\{" ++ ((reverse . (drop 1) . reverse)
      (foldr (\x -> \y -> latex x ++ "," ++ y) "" ts)) ++ "\\}"
  latex (MinBra ts) =
    "\\&" ++ "\\{" ++ ((reverse . (drop 1) . reverse)
      (foldr (\x -> \y -> latex x ++ "," ++ y) "" ts))++ "\\}"
  latexSuppresTypes x = latex x


instance Latex MinU where
  latex (MinULin ts) =
    ((reverse . (drop 1) . reverse)
      (foldr (\x -> \y ->latex x ++ "," ++ y) "" ts))  ++ "\\multimap \\diamond "
  latex (MinUSh ts) =
    ((reverse . (drop 1) . reverse)
      (foldr (\x -> \y ->latex x ++ "," ++ y) "" ts))  ++ "\\to \\diamond "
  latexSuppresTypes x = latex x


--------------------------------------------------------------------------------
-- Generating latex document
--------------------------------------------------------------------------------

ifProp :: ChannelN -> Bool
ifProp (Ch c) = c!!0 == 'c'
ifProp (ChCmpl c) = c!!0 == 'c'

zcolor :: ChannelN -> String
zcolor z = if (ifProp z)
            then
              "red"
            else
              "black"

latexTrios :: PProc -> String
latexTrios (PSend u v p) =
  let color = zcolor u in
      let str1 = if (length v == 0) then
                    ""
                  else
                     "[" ++ ((reverse . (drop 1) . reverse)
                     (foldr (\x-> \y-> latexTriosS x ++ "," ++ y) "" v)) ++ "]"
          in
            "\\color{" ++ color ++"}" ++ latexSuppresTypes u  ++ "!" ++
            " \\langle " ++ str1 ++ " \\rangle " ++ "\\color{black}" ++
            "." ++ latexTrios p

latexTrios (PRecv u x t p) =
  let str1 = if (length x == 0) then
                ""
              else
                "[" ++ ((reverse . (drop 1) . reverse)
                  (foldr (\x-> \y-> x ++ "," ++ y) "" x)) ++ "]"
      in
         "\\color{" ++ (zcolor u) ++"}" ++ latexSuppresTypes u ++  "?" ++
         "(" ++ str1 ++ ")."++ "\\color{black}" ++ latexTrios p

latexTrios (PApp v u) =
  latexTriosV v ++ " \\ " ++ "(" ++ ((reverse . (drop 1) .  reverse)
    (foldr (\x-> \y-> latexSuppresTypes x ++ "," ++ y) "" u)) ++ ")"
latexTrios (PAppVar x u) =
  x ++ " \\ " ++ "[" ++ ((reverse . (drop 1) . reverse)
  (foldr (\x-> \y-> latexSuppresTypes x ++ "," ++ y) "" u)) ++ "]"
latexTrios (PComp pl) =
  "(" ++ drop 8 xs ++ ")"
    where
      xs = (foldr ((++) . (++) (" \\ | \\\\ ") . latexTrios) "" pl)
latexTrios (PRestr nts p) =
  if (length nts > 5) then
      " (" ++ "\\nu "  ++ (latexSuppresTypesS (map fst nts))  ++ ")" ++
      " \\\\ " ++ "(" ++ latexTrios p ++ ")"
    else
      " (" ++ "\\nu "  ++ (latexSuppresTypesS (map fst nts))  ++ ")"  ++
      "(" ++ latexTrios p ++ ")"
latexTrios PEnd = "0 "
latexTrios (PBinOp op x y xy p) =
  latex x ++ " " ++ latexSuppresTypes op ++ " " ++ latex y ++ " \\to " ++
  latex xy ++ "." ++ latexSuppresTypes p
latexTrios (PSelect u l p) =
  latexSuppresTypes u ++ "<" ++ "l" ++ "_" ++ "{" ++ show l ++ "}" ++
  "." ++ latexSuppresTypes p
latexTrios (PBranch u ps) =
  latexSuppresTypes u ++ ">" ++ "\\{" ++
  (drop 3 (foldr ((++) . (++) (" \\ | \\ ") . latexSuppresTypes) "" ps)) ++"\\}"

latexTriosV :: PAbstr -> String
latexTriosV (PAbstr x t p) =
  " \\lambda " ++ "[" ++ ((reverse . (drop 1) . reverse)
  (foldr (\x-> \y-> latexSuppresTypes x ++ "," ++ y) "" x)) ++ "]" ++
  "." ++ latexTrios p

latexTriosS :: PSendee -> String
latexTriosS (SAbstr v) = latexTriosV v
latexTriosS (SVar x) = x

preamb =
  "\\documentclass{article}\n\\usepackage[utf8]{inputenc}\n\\usepackage[fleqn]{amsmath}\n"++
  "\\usepackage{color}\n\\usepackage{mathtools}\n" ++ "\\title{Misty output}"
  ++"\\begin{document}\n\\maketitle\n\\paragraph{Input HO process}\n"

getPreamb :: String -> String
getPreamb name =
    "\\documentclass{article}\n\\usepackage[utf8]{inputenc}\n\\usepackage[fleqn]{amsmath}\n" ++
    "\\usepackage{color}\n\\usepackage{mathtools}\n" ++
    "\\newcommand{\\misty}{\\textsf{MISTY}}\n" ++
    "\\title{\\misty \\ output: " ++ "\\texttt" ++ "{" ++ name ++ "}"++ "}\n" ++
    "\\begin{document}\n\\maketitle\n\\paragraph{Input HO process}\n"

printPLatex :: String -> Proc -> String
printPLatex name p = let ps = latexSuppresTypes p in
                        "\\paragraph{" ++ name ++ "}" ++ "\n" ++ "\\begin{align*}\n" ++
                        ps ++ "\n" ++ "\\end{align*}" ++ "\n"

printPs :: Int -> [Proc] -> String
printPs n [] = ""
printPs n ps@(x:xs) = (printPLatex ("R" ++ show n) x) ++ (printPs (n+1) xs)

printReduct :: String -> PProc -> String
printReduct name p = let ps = latexTrios p in
                      "\\paragraph{" ++ name ++ "}" ++ "\n" ++ "\\begin{align*}\n" ++
                      ps ++ "\n" ++ "\\end{align*}" ++ "\n"

printReducts :: [PProc] -> String
printReducts procs = foldr (\x -> \y -> (printReduct "" x) ++ "\n" ++ y) "" procs

printReducts' :: Int -> [PProc] -> String
printReducts' n [] = ""
printReducts' n ps@(x:xs) =
  (printReduct ("R" ++ show (n)) x) ++ (printReducts' (n+1) xs)

getLatexContent' :: String -> [Proc] -> [PProc] -> String
getLatexContent' preamb procs pprocs =
              preamb ++ "\n" ++ (printPs 0 procs) ++ "\n" ++
              "\\paragraph{Decomposed HO process}" ++ (printReducts' 0 pprocs) ++
              "\n" ++ "\\end{document}"

getLatexContent :: String -> [Proc] -> [PProc] -> String
getLatexContent name procs pprocs =
              preamb' ++ "\n" ++ (printPs 0 procs) ++ "\n" ++
              "\\paragraph{Decomposed HO process}" ++ (printReducts' 0 pprocs) ++
              "\n" ++ "\\end{document}"
                where
                  preamb' = getPreamb name
