--  Copyright (c) 2019. Alen Arslanagic, Jorge A. Perez and Erik Voogd
--  and University of Groningen.
--  All rights reserved.
--
--  Redistribution and use in source and binary forms, with or without
--  modification, are permitted provided that the following conditions are met:
--
--  * Redistributions of source code must retain the above copyright notice,
--    this list of conditions and the following disclaimer.
--
--  * Redistributions in binary form must reproduce the above copyright notice,
--    this list of conditions and the following disclaimer in the documentation
--    and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,  THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES;  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON  ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF  THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.


{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE GADTs #-}

module Misty.Decomposition where

import Misty.Process
import Misty.Types
import Misty.Channel
import Misty.Latex
import Misty.DecompositionTypes
import Misty.DecompositionBase

import Control.Monad.Except as Except

import qualified Data.List as List
import qualified Data.Map as Map
import qualified Data.Set as Set

import Data.Char

-- Monadic computation
runDecompExcept :: Proc -> [(ChannelN, TSession)] -> ExceptDecomp PProc
runDecompExcept p env = do
                          p2 <- decompProc (decompInit p) 1 env []
                          return $ PRestr cs' $ PComp $ (p1:p2)
                            where
                              cs = xtuple (Ch "c") (lengthProc p)
                              cs' = zip cs (replicate (length cs) MinEnd)
                              p1 = PSend (getc 1 False) [] PEnd

runDecomp :: Proc -> [(ChannelN, TSession)] -> PProc
runDecomp p env = case runExcept $ runDecompExcept p env of
                    Right proc -> proc
                    Left err -> PEnd


--------------------------------------------------------------------------------
-- CORE FRAGMENT DECOMPOSITION
--------------------------------------------------------------------------------

decompProc :: Proc -> Int -> [(ChannelN, TSession)] ->
                [(VarN, MinU)] -> ExceptDecomp [PProc]
decompProc (Send ui v q) k env xprops =
    do
        v' <- decompValue (incr_v) (k+1) env yprops
        t2 <- decompProc incr_q (k+r+1) env' zprops
        let t1 = PRecv (getc k True) xs txs
                    $ PSend ui [SAbstr v']
                        $ PSend (getc (k+r+1) False)
                            (map (\x -> SVar x) zs) PEnd in
            return $ [t1] ++ t2
            where
                (yprops, zprops) = txsplit xprops (ftvv v) (ftvv q)
                (ys, tys) = unzip yprops
                (zs, tzs) = unzip zprops
                (xs, txs) = unzip xprops
                r = lengthAbs v
                subs = SubstN $ Map.fromList [(ui, succName ui)]
                incr_v = applyn subs v
                incr_q = incrName ui q
                env' = reduceEnv env ui

decompProc (SendVar ui y q) k env xprops  =
    do
        t2 <- decompProc incr_q (k+1) env' zprops
        return $ [t1] ++ t2
          where
            zprops = txdel xprops y
            (xs, txs) = unzip xprops
            (zs, tzs) = unzip zprops
            incr_q = incrName ui q
            env' = reduceEnv env ui
            t1 = PRecv (getc k True) (xs) (txs)
                  $ PSend ui [SVar y]
                    $ PSend (getc (k+1) False)
                        (map (\x -> SVar x) zs) PEnd


decompProc (Recv ui t y q) k env xprops =
    do
        t2 <- decompProc (incrName ui q) (k+1) env' zprops
        return $ [t1] ++ t2
          where
            decompt@(MinULin decomps) = typeDecompAbs t
            (xs, txs) = unzip xprops
            zprops = xprops ++ [(y,decompt)]
            (zs, tzs) = unzip zprops
            env' = reduceEnv env ui
            t1 = PRecv (getc k True) (xs) (txs)
                  $ PRecv ui [y] [decompt]
                    $ PSend (getc (k+1) False) (map (\x -> SVar x) zs) PEnd

decompProc (App v ui) k env xprops =
    case v of
      (Abstr n t p) ->
        do
            v' <- decompValue v (k+1) env xprops
            return $ [PRecv (getc k True) xs txs $ PApp v' us]
                          where
                            (xs,txs) = unzip xprops
                            decompt = typeDecomp t
                            r = length decompt
                            us = utupleto r ui

decompProc (AppVar y ui) k env xprops =
    case runExcept $ getType ui env of
        Right t' -> return $ [PRecv (getc k True) xs txs $ PAppVar y us]
                        where
                            r = length (typeDecomp t')
                            us = utupleto r ui
                            (xs, txs) = unzip xprops
        Left err -> throwError err

decompProc (Comp pl) k env xprops =
  case pl of
      [] -> return $ []
      (p:[]) -> decompProc p k env xprops
      (p:ps) -> do
                    t2 <- decompProc p (k+1) env xpprops
                    t3 <- decompProc (Comp ps) (k+r+1) env xpsprops
                    return $ [t1] ++ t2 ++ t3
                  where
                    (xpprops, xpsprops) = txsplit xprops (ftvv p)
                                            (ftvv (Comp ps))
                    r = lengthProc p
                    (xs, txs) = unzip xprops
                    (xp, txp) = unzip xpprops
                    (xps, txps) = unzip xpsprops
                    t1 = PRecv (getc k True) (xs) (txs)
                              $ PSend (getc (k+1) False)
                                      (map (\x -> SVar x) xp)
                                $ PSend (getc (k+r+1) False)
                                        (map (\x -> SVar x) xps) PEnd

decompProc (Restr s t p) k env xprops =
    do
        t1 <- decompProc p'' k env' xprops
        return $ [PRestr (zip stuple decomp) (PComp t1)]
            where
            s' = succName (succName s)
            decomp = typeDecomp t
            p' = (substituteName s s' p)
            p'' = (substituteName (compl s) (succName (succName (compl s))) p')
            env' = [(s,t),(compl s, dualType t)] ++ env
            stuple = xtuple s (length decomp)

decompProc End k env xprops =
  return $ [t1]
    where
      (xs,txs) = unzip xprops
      t1 = PRecv (getc k True) xs txs PEnd

decompProc (BinOp op x y xy p) k env xprops =
  do
    p' <- decompProc p k env xprops
    return $ [PBinOp op x y xy $ PComp $ p']

decompProc (AppP v us) k env xprops =
  do
    v' <- decompValue v (k+1) env xprops
    return $ [PRecv (getc k True) xs txs $ (PApp v' (ms))]
        where
            mts = filter (\(ui,ti) -> List.elem ui us) env
            (gus, gts) = unzip mts
            (xs,txs) = unzip xprops
            ms = produce_ms gts gus

decompProc (AppVarP y us) k env xprops =
  do
    return $ [PRecv (getc k True) xs txs $ (PAppVar y (ms))]
        where
            mts = map (\ui -> List.find (\(u1,t1) ->
                      u1 == getBindChannelName ui) env) us
            ts = map (\mt -> case mt of
                                  Just (u',t) -> (u',t)
                                  Nothing -> (Ch "",TEnd)) mts
            (gus, gts) = unzip ts
            (xs,txs) = unzip xprops
            ms = produce_ms gts gus

-- SELECT AND BRANCH
decompProc (Branch ui ps) k env xprops =
  case runExcept $ getType ui env of
    Right t' ->
      case t' of
          TBra ts ->
            do
              ptrios <- mapM f (zip ps [1..(length ps)])
              return $ [PRecv (getc k True) xs txs $ PBranch ui ptrios]
                where
                  (xs,txs) = unzip xprops
                  f :: (Proc, Int) -> ExceptDecomp PProc
                  f (x,y) =
                    do
                      p2 <- decompProc x (k+1) (reduceEnv env ui) xprops
                      let  nj = PAbstr us decomp
                                  $ PRestr (zip cs (replicate (length cs) MinEnd))
                                    $ PComp ([p1]++p2) in
                        return $ PSend ui [SAbstr nj] PEnd
                          where
                            (xs,txs) = unzip xprops
                            p1 = PSend (getc (k+1) False)
                                        (map (\x -> SVar x) xs) PEnd
                            x' = substituteName (ui) (succName ui) x
                            cs = utupleto ((lengthProc x)+(k)) (Ch cname)
                            cname = "c" ++ (show (k+1))
                            us = utupleto (length decomp) (ui)
                            decomp = typeDecomp (ts!!(y-1))
          _ -> throwError NoTypeBinding
    Left error -> throwError error

decompProc (Select ui l q) k env xprops =
  case runExcept $ getType ui env of
    Right t' ->
      case t' of
          TSl sl ->
            do
              q2 <- decompProc (incrName ui q) (k+2) env xprops
              let p2 = PRestr (zip us decomp) $ PComp ([q1]++q2) in
                return $ [p1,p2]
                        where
                          (xs,txs) = unzip xprops
                          p1 = PRecv (getc k True) xs txs
                                $ PSend (getc (k+1) False) [SAbstr mj] PEnd
                          q1 = PRecv (getc (k+1) True)
                                  ["y"] [] $ PAppVar "y" uscompl
                          mj = PAbstr zs [] $ PSelect ui l
                                  $ PRecv ui ["z"] []
                                    $ PSend (getc (k+2) False)
                                        (map (\x -> SVar x) xs)
                                        $ PAppVar "z" zs
                          zs = utupleto (length decomp) (Ch "y")
                          uscompl = map compl us
                          us = utupleto ((length decomp)+(fst (getkChannel ui)))
                                  (succName ui)
                          decomp = typeDecomp (sl!!l)
          _ -> throwError NoTypeBinding
    Left err -> throwError err


decompValue :: Abstr -> Int -> [(ChannelN, TSession)] ->
                [(VarN, MinU)] -> ExceptDecomp PAbstr
decompValue (Abstr u t p) k env xprops =
    do
        t2 <- decompProc p' k env' xprops
        return $ PAbstr us ts $ PComp $ [t1] ++ t2
            where
                (xs,txs) = unzip xprops
                env' = [(u, t)] ++ env
                ts = typeDecomp t
                us = xtuple (Ch "y") (length ts)
                p' = substituteName u (head us) p
                t1 = PSend (getc k False) (map (\x -> SVar x) xs) PEnd

decompValue (AbstrP us ts p) k env xprops =
    do
        t2 <- decompProc p' k env' xprops
        return $ PAbstr (concat uss) (concat tss) $ PComp $ [t1] ++ t2
        where
            (xs,txs) = unzip xprops
            us' = map (succName . succName) us
            env' = (zip us ts) ++ env
            tss = map typeDecomp ts
            uss = map (\(u,ts) -> xtuple u (length ts)) $ zip us tss
            subs = SubstN $ Map.fromList $ zip us us'
            p' = applyn subs p
            t1 = PSend (getc k False) (map (\x -> SVar x) xs) PEnd


produce_ms :: [TSession] -> [ChannelN] -> [ChannelN]
produce_ms ts us = foldr (\(si,ui) -> \b ->
                    (utupleto (length $ typeDecomp si) ui) ++ b) [] (zip ts us)
