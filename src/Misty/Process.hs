--  Copyright (c) 2019. Alen Arslanagic, Jorge A. Perez and Erik Voogd
--  and University of Groningen.
--  All rights reserved.
--
--  Redistribution and use in source and binary forms, with or without
--  modification, are permitted provided that the following conditions are met:
--
--  * Redistributions of source code must retain the above copyright notice,
--    this list of conditions and the following disclaimer.
--
--  * Redistributions in binary form must reproduce the above copyright notice,
--    this list of conditions and the following disclaimer in the documentation
--    and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,  THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES;  LOSS OF USE, DATA, OR PROFITS; # OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON  ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF  THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.


{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE DeriveFunctor #-}


module Misty.Process where

import Misty.Types
import Misty.Channel

import Control.Monad.State as State
import Control.Monad.Except as Except
import Control.Monad.Identity as Identity
import Control.Monad.Free

import qualified Data.List as List
import qualified Data.Map as Map
import qualified Data.Set as Set

import Data.Char

--------------------------------------------------------------------------
-- MONADIC PROCESS TARGET LANGUAGE
--------------------------------------------------------------------------
newtype SubstN = SubstN (Map.Map ChannelN ChannelN)
newtype SubstAbs = SubstAbs (Map.Map VarN Abstr)

class SubstitableN a where
  applyn :: SubstN -> a -> a
  ftvn  :: a -> Set.Set ChannelN

class SubstitableAbs a where
  applyAbs  :: SubstAbs -> a -> a
  ftvv      :: a -> Set.Set VarN

data Abstr = Abstr ChannelN TSession Proc | AbstrP [ChannelN] [TSession] Proc
  deriving (Eq, Ord)

data Proc =
    Send ChannelN Abstr Proc
  | Recv ChannelN TAbs VarN Proc
  | App Abstr ChannelN
  | AppVar VarN ChannelN
  | AppP Abstr [ChannelN]
  | AppVarP VarN [ChannelN]
  | Comp [Proc]
  | SendVar ChannelN VarN Proc
  | Restr ChannelN TSession Proc
  | Select ChannelN Int Proc
  | Branch ChannelN [Proc]
  | BinOp Op ChannelN ChannelN ChannelN Proc
  | End
  deriving (Eq, Ord)

instance SubstitableN Proc where
  applyn s@(SubstN s') (Send u v p) =
      Send (Map.findWithDefault u u s') (applyn s v) (applyn s p)
  applyn s@(SubstN s') (SendVar u y p) =
      SendVar (Map.findWithDefault u u s') y (applyn s p)
  applyn s@(SubstN s') (Recv u t x p) =
      Recv (Map.findWithDefault u u s') t x (applyn s p)
  applyn s@(SubstN s') (App v u) =
      App (applyn s v) (Map.findWithDefault u u s')
  applyn s@(SubstN s') (AppVar x u) =
      AppVar x (Map.findWithDefault u u s')
  applyn s@(SubstN s') (AppP v us) =
      AppP (applyn s v) $ map (\u -> Map.findWithDefault u u s') us
  applyn s@(SubstN s') (AppVarP x us) =
      AppVarP x $ map (\u -> Map.findWithDefault u u s') us
  applyn s (Comp pl) = Comp $ map (applyn s) pl
  applyn (SubstN s) (Restr n t p) =
      Restr n t (applyn s' p)
        where s' = SubstN $ Map.delete n s
  applyn s@(SubstN s') (Select u l p) =
      Select (Map.findWithDefault u u s') l (applyn s p)
  applyn s@(SubstN s') (Branch u ps) =
      Branch (Map.findWithDefault u u s') (map (applyn s) ps)
  applyn _ End = End
  applyn s@(SubstN s') (BinOp op x y xy p) =
      BinOp op (Map.findWithDefault x x s')
                (Map.findWithDefault y y s')
                (Map.findWithDefault xy xy s')
                (applyn s p)
  ftvn (Send u v p) = Set.union (ftvn v) (u `Set.insert` (ftvn p))
  ftvn (SendVar u y p) = u `Set.insert` (ftvn p)
  ftvn (Recv u t x p) = u `Set.insert` (ftvn p)
  ftvn (App v u) = u `Set.insert` (ftvn v)
  ftvn (AppVar x u) = Set.singleton u
  ftvn (AppP v us) = (Set.fromList us) `Set.union` (ftvn v)
  ftvn (AppVarP x us) = Set.fromList us
  ftvn (Comp pl) = foldr (Set.union . ftvn) Set.empty pl
  ftvn (Restr n t p) = Set.delete (compl n) (Set.delete n (ftvn p))
  ftvn End = Set.empty
  ftvn (Select u l p) = Set.insert u (ftvn p)
  ftvn (Branch u ps) = Set.insert u $ foldr (Set.union . ftvn) Set.empty ps
  ftvn (BinOp op x y xy p) = Set.insert x $ Set.insert y $ Set.insert xy $ ftvn p

instance SubstitableN Abstr where
  applyn (SubstN s) (Abstr x t p) =
    Abstr x t (applyn s' p)
      where s' = SubstN (Map.delete x s)
  applyn (SubstN s) (AbstrP xs ts p) =
    AbstrP xs ts (applyn s' p)
      where s' = SubstN $ foldr Map.delete s (xs ++ (map compl xs))
  ftvn (Abstr x t p) = Set.delete (compl x) (Set.delete x (ftvn p))
  ftvn (AbstrP xs ts p) = foldr Set.delete (ftvn p) (xs ++ (map compl xs))


instance SubstitableAbs Proc where
  applyAbs s@(SubstAbs s') (Send u v p) = Send u (applyAbs s v) (applyAbs s p)
  applyAbs s@(SubstAbs s') (SendVar u y p) =
      case (Map.lookup y s') of
          Just v -> Send u v (applyAbs s p)
          Nothing -> SendVar u y (applyAbs s p)
  applyAbs s@(SubstAbs s') (Recv u t x p) = Recv u t x (applyAbs s'' p)
                                            where s'' = SubstAbs $ Map.delete x s'
  applyAbs s@(SubstAbs s') (App v u) = App (applyAbs s v) u
  applyAbs s@(SubstAbs s') (AppP v us) = AppP (applyAbs s v) us
  applyAbs s@(SubstAbs s') (AppVar x u)  = case (Map.lookup x s') of
                                              Just v -> App v u
                                              Nothing -> AppVar x u
  applyAbs s@(SubstAbs s') (AppVarP x us)  = case (Map.lookup x s') of
                                              Just v -> AppP v us
                                              Nothing -> AppVarP x us
  applyAbs s (Comp pl) = Comp $ map (applyAbs s) pl
  applyAbs s@(SubstAbs s') (BinOp op x y xy p) = BinOp op x y xy (applyAbs s p)
  applyAbs s@(SubstAbs s') (Restr n t p) = Restr n t (applyAbs s p)
  applyAbs s@(SubstAbs s') (Select u l p) = Select u l (applyAbs s p)
  applyAbs s@(SubstAbs s') (Branch u ps) = Branch u $ map (applyAbs s) ps
  applyAbs _ End = End
  ftvv (Send u v p) = Set.union (ftvv v) (ftvv p)
  ftvv (SendVar u y p) = Set.union (Set.singleton y) (ftvv p)
  ftvv (Recv u t x p) = Set.delete x (ftvv p)
  ftvv (App v u) =  (ftvv v)
  ftvv (AppP v us) =  (ftvv v)
  ftvv (AppVar x u) = Set.singleton x
  ftvv (AppVarP x us) = Set.singleton x
  ftvv (Comp pl) = foldr (Set.union . ftvv) Set.empty pl
  ftvv (Restr n t p) = ftvv p
  ftvv End = Set.empty
  ftvv (Select u l p) = ftvv p
  ftvv (Branch u ps) = foldr (Set.union . ftvv) Set.empty ps
  ftvv (BinOp op x y xy p) = ftvv p

instance SubstitableAbs Abstr where
  applyAbs s@(SubstAbs s') (Abstr x t p) = Abstr x t (applyAbs s p)
  applyAbs s@(SubstAbs s') (AbstrP xs ts p) = AbstrP xs ts (applyAbs s p)
  ftvv (Abstr x t p)  = ftvv p
  ftvv (AbstrP xs ts p)  = ftvv p

--------------------------------------------------------------------------
-- POLYADIC HO PROCESS OUTPUT LANGUAGE WITH MINIMAL TYPES
--------------------------------------------------------------------------
data PAbstr = PAbstr [ChannelN] [MinM] PProc
  deriving (Eq, Ord)


data PSendee = SAbstr PAbstr | SVar VarN
  deriving (Eq,Ord)


data PProc =
    PSend ChannelN [PSendee] PProc
  | PRecv ChannelN [VarN] [MinU] PProc
  | PApp PAbstr [ChannelN]
  | PAppVar VarN [ChannelN]
  | PComp [PProc]
  | PSelect ChannelN Int PProc
  | PBranch ChannelN [PProc]
  | PRestr [(ChannelN, MinM)] PProc
  | PBinOp Op ChannelN ChannelN ChannelN PProc
  | PEnd
  deriving (Eq,Ord)


newtype SubstPolyAbs = SubstPolyAbs (Map.Map VarN PAbstr)


class SubstitablePolyAbs a where
  applyPolyAbs  :: SubstPolyAbs -> a -> a
  ftvpv         :: a -> Set.Set VarN
  ftvpvl        :: a -> [VarN]


instance SubstitableN PSendee where
  applyn s@(SubstN s') (SAbstr v) = SAbstr $ applyn s v
  applyn s@(SubstN s') (SVar x) = SVar x
  ftvn (SAbstr v) = ftvn v
  ftvn (SVar x) = Set.empty


instance SubstitableN PProc where
  applyn s@(SubstN s') (PSend u vs p) = PSend (Map.findWithDefault u u s')
                                      (map (applyn s) vs)
                                      (applyn s p)
  applyn s@(SubstN s') (PBinOp op x y xy p) =
    PBinOp op (Map.findWithDefault x x s')
                (Map.findWithDefault y y s')
                (Map.findWithDefault xy xy s')
                (applyn s p)
  applyn s@(SubstN s') (PRecv u xs ts p) =
    PRecv (Map.findWithDefault u u s') xs ts (applyn s p)
  applyn s@(SubstN s') (PApp v us) =
    PApp (applyn s v) $ map (\u -> Map.findWithDefault u u s') us
  applyn s@(SubstN s') (PAppVar x us)  =
    PAppVar x $ map (\u -> Map.findWithDefault u u s') us
  applyn s (PComp pl) = PComp $ map (applyn s) pl
  applyn (SubstN s) (PRestr nts p) =
    PRestr nts (applyn s' p)
      where s' = SubstN $ foldr Map.delete s (map fst nts)
  applyn s@(SubstN s') (PSelect u l p) =
    PSelect (Map.findWithDefault u u s') l (applyn s p)
  applyn s@(SubstN s') (PBranch u ps) =
    PBranch (Map.findWithDefault u u s') (map (applyn s) ps)

  applyn _ PEnd = PEnd
  ftvn (PSend u vs p) =
    Set.union (foldr (Set.union . ftvn) Set.empty vs) (u `Set.insert` (ftvn p))
  ftvn (PRecv u ts xs p) = u `Set.insert` (ftvn p)
  ftvn (PApp v us) = Set.union (Set.fromList us) (ftvn v)
  ftvn (PAppVar x us) = Set.fromList us
  ftvn (PComp pl) = foldr (Set.union . ftvn) Set.empty pl
  ftvn (PRestr nts p) =
    foldr Set.delete (ftvn p) ((map fst nts) ++ (map compl (map fst nts)))
  ftvn (PSelect u l p) = Set.insert u (ftvn p)
  ftvn (PBranch u ps) = Set.insert u $ foldr (Set.union . ftvn) Set.empty ps
  ftvn (PBinOp op x y xy p) =
    Set.insert x $ Set.insert y $ (Set.insert xy $ ftvn p)
  ftvn PEnd = Set.empty


instance SubstitableN PAbstr where
  applyn (SubstN s) (PAbstr xs ts p) = PAbstr xs ts (applyn s' p)
                                  where s' = SubstN $ foldr Map.delete s xs
  ftvn (PAbstr xs t p) = foldr Set.delete (ftvn p) (xs ++ (map compl xs))


instance SubstitablePolyAbs PSendee where
  applyPolyAbs s@(SubstPolyAbs s') (SAbstr v) = SAbstr $ applyPolyAbs s v
  applyPolyAbs s@(SubstPolyAbs s') (SVar x)   = case (Map.lookup x s') of
                                                  Just v -> SAbstr v
                                                  Nothing -> SVar x
  ftvpv (SAbstr v) = ftvpv v
  ftvpv (SVar x) = Set.singleton x
  ftvpvl (SAbstr v) = ftvpvl v
  ftvpvl (SVar x) = [x]

instance SubstitablePolyAbs PProc where
  applyPolyAbs s@(SubstPolyAbs s') (PSend u vs p) =
    PSend u (map (applyPolyAbs s) vs) (applyPolyAbs s p)
  applyPolyAbs s@(SubstPolyAbs s') (PRecv u xs ts p) =
    PRecv u xs ts (applyPolyAbs s'' p)
      where s'' = SubstPolyAbs $ foldr Map.delete s' xs
  applyPolyAbs s@(SubstPolyAbs s') (PApp v us) = PApp (applyPolyAbs s v) us
  applyPolyAbs s@(SubstPolyAbs s') (PAppVar x us)  =
    case (Map.lookup x s') of
        Just v -> PApp v us
        Nothing -> PAppVar x us
  applyPolyAbs s (PComp pl) = PComp $ map (applyPolyAbs s) pl
  applyPolyAbs s@(SubstPolyAbs s') (PRestr nts p) =
    PRestr nts (applyPolyAbs s p)
  applyPolyAbs _ PEnd = PEnd
  applyPolyAbs s@(SubstPolyAbs s') (PSelect u l p) =
    PSelect u l (applyPolyAbs s p)
  applyPolyAbs s@(SubstPolyAbs s') (PBranch u ps) =
    PBranch u $ map (applyPolyAbs s) ps
  applyPolyAbs s@(SubstPolyAbs s') (PBinOp op x y xy p) =
    PBinOp op x y xy (applyPolyAbs s p)

  ftvpv (PSend u vs p) =
    Set.union (foldr (Set.union . ftvpv) Set.empty vs) (ftvpv p)
  ftvpv (PRecv u xs ts p) = foldr Set.delete (ftvpv p) xs
  ftvpv (PApp v u) =  (ftvpv v)
  ftvpv (PAppVar x u) = Set.singleton x
  ftvpv (PComp pl) = foldr (Set.union . ftvpv) Set.empty pl
  ftvpv (PRestr nts p) = ftvpv p
  ftvpv (PSelect u l p) = ftvpv p
  ftvpv (PBranch u ps) = foldr (Set.union . ftvpv) Set.empty ps
  ftvpv (PBinOp op x y xy p) = ftvpv p
  ftvpv PEnd = Set.empty

  ftvpvl (PSend u vs p)     = (foldr ((++) . ftvpvl) [] vs) ++ (ftvpvl p)
  ftvpvl (PRecv u xs ts p)  = foldr List.delete (ftvpvl p) xs
  ftvpvl (PApp v u)         = ftvpvl v
  ftvpvl (PAppVar x u)      = [x]
  ftvpvl (PComp pl)         = foldr ((++) . ftvpvl) [] pl
  ftvpvl (PRestr nts p)     = ftvpvl p
  ftvpvl (PSelect u l p)    = ftvpvl p
  ftvpvl (PBranch u ps)     = foldr ((++) . ftvpvl) [] ps
  ftvpvl (PBinOp op x y xy p) = ftvpvl p
  ftvpvl PEnd               = []

instance SubstitablePolyAbs PAbstr where
  applyPolyAbs s@(SubstPolyAbs s') (PAbstr xs ts p) = PAbstr xs ts (applyPolyAbs s p)
  ftvpv (PAbstr xs t p)   = ftvpv p
  ftvpvl (PAbstr xs t p)  = ftvpvl p

substituteName :: ChannelN -> ChannelN -> Proc -> Proc
substituteName u n p = applyn (SubstN $ Map.fromList [(u,n)]) p


--------------------------------------------------------------------------
-- MONADIC PROCESS INPUT LANGUAGE
--------------------------------------------------------------------------
data Op = OpAdd | OpSub
          deriving (Eq, Ord)

data AbstrB  = AbstrB ST (ChannelN -> ProcF ()) | PAbstrB [ST] ([ChannelN] -> ProcF ())

data ProcB next =
  RecvB ChannelN STAbs (VarN -> next)
  | SendB ChannelN AbstrB next
  | PAppB AbstrB [ChannelN]
  | AppB AbstrB ChannelN
  | AppVarB VarN ChannelN
  | PAppVarB VarN [ChannelN]
  | CompB [next]
  | SendVarB ChannelN VarN next
  | RestrB ST (ChannelN -> next)
  | SelectB ChannelN Int next
  | BranchB ChannelN [next]
  | BinOpB Op ChannelN ChannelN (ChannelN -> next)
  deriving (Functor)

type ProcF = Free ProcB

add :: ChannelN -> ChannelN -> ProcF ChannelN
add x y = Free (BinOpB OpAdd x y (\a -> Pure a))

sub :: ChannelN -> ChannelN -> ProcF ChannelN
sub x y = Free (BinOpB OpSub x y (\a -> Pure a))

binop :: Op -> ChannelN -> ChannelN -> ProcF ChannelN
binop op x y = Free (BinOpB op x y (\a -> Pure a))

receive :: ChannelN -> STAbs -> ProcF VarN
receive u t = Free (RecvB u t (\a -> Pure a))

new :: ST -> ProcF ChannelN
new t = Free (RestrB t (\a -> Pure a))

send :: ChannelN -> AbstrB -> ProcF ()
send u v = Free (SendB u v (Pure ()))

sendvar :: ChannelN -> VarN -> ProcF ()
sendvar u v = Free (SendVarB u v (Pure ()))

app :: AbstrB -> ChannelN -> ProcF ()
app v u = Free (AppB v u)

papp :: AbstrB -> [ChannelN] -> ProcF ()
papp v us = Free (PAppB v us)

appvar :: VarN -> ChannelN -> ProcF ()
appvar v u = Free (AppVarB v u)

pappvar :: VarN -> [ChannelN] -> ProcF ()
pappvar v us = Free (PAppVarB v us)

par :: [ProcF ()] -> ProcF ()
par ps = Free (CompB ps)

select :: ChannelN -> Int -> ProcF ()
select u l = Free $ SelectB u l (Pure ())

branch :: ChannelN -> [ProcF ()] -> ProcF ()
branch u ps = Free $ BranchB u ps

abstr :: ST -> (ChannelN -> ProcF ()) -> AbstrB
abstr t f = AbstrB t f

pabstr :: [ST] -> ([ChannelN] -> ProcF ()) -> AbstrB
pabstr ts f = PAbstrB ts f

end :: ProcF ()
end = Pure ()

type FreshM a = State Unique a

data Unique = Unique {count :: Int}
  deriving (Show)

data Uniques = Uniques {xcount :: Int, scount :: Int}
  deriving (Show)

type FreshMs a = State Uniques a

initUniques :: Uniques
initUniques = Uniques {xcount = 0, scount = 0}

freshx :: FreshMs String
freshx =
  do
    x <- get
    put x{xcount = xcount x + 1}
    return (letters !! xcount x)


freshs :: FreshMs String
freshs =
  do
    s <- get
    put s{scount = scount s + 1}
    return ("s" ++ "^{" ++ show (scount s) ++ "}")

letters :: [String]
letters = [1..] >>= flip replicateM ['x','y','w','v']

initUnique :: Unique
initUnique = Unique { count = 0 }


interpretAbsToS :: AbstrB -> FreshMs Abstr
interpretAbsToS (AbstrB t fp) =
    do
      x <- freshx
      next' <- interpretToS (fp (Ch x))
      return $ Abstr (Ch x) (interpretT t) next'
interpretAbsToS (PAbstrB ts fp) =
  do
    xs <- forM ts (\_->freshx)
    next' <- interpretToS (fp (map (\x -> Ch x) xs))
    return $ AbstrP (map (\x -> Ch x) xs) (map interpretT ts) next'

combineChannels :: Op -> ChannelN -> ChannelN -> ChannelN
combineChannels op (Ch n1) (Ch n2) = case op of
                                        OpAdd -> Ch ("p" ++ n1 ++ n2)
                                        OpSub -> Ch ("m" ++ n1 ++ n2)
combineChannels op (ChCmpl n1) (ChCmpl n2) = case op of
                                            OpAdd -> ChCmpl ("p" ++ n1 ++ n2)
                                            OpSub -> ChCmpl ("m" ++ n1 ++ n2)

interpretToS :: ProcF () -> FreshMs Proc
interpretToS (Free (RecvB u t fnext)) =
      do
        x <- freshx
        next' <- interpretToS (fnext x)
        return $ Recv u (interpretTAbs t) x next'
interpretToS (Free (BinOpB op x y fnext)) =
  do
    next' <- interpretToS $ fnext $ combineChannels op x y
    return $ BinOp op x y (combineChannels op x y) next'

interpretToS (Free (SendB u v next)) =
      do
        v' <- interpretAbsToS v
        next' <- interpretToS next
        return $ Send u v' next'
interpretToS (Free (PAppB v us)) =
  do
    v' <- interpretAbsToS v
    return $ AppP v' us
interpretToS (Free (AppB v u)) =
      do
        v' <- interpretAbsToS v
        return $ App v' u
interpretToS (Free (PAppVarB x us)) =
    return $ AppVarP x us
interpretToS (Free (AppVarB x v)) =
    return $ AppVar x v
interpretToS (Free (CompB ps)) =
      do
        ps' <- mapM interpretToS ps
        return $ Comp ps'
interpretToS (Free (SendVarB u x next)) =
      do
        next' <- interpretToS next
        return $ (SendVar u x next')
interpretToS (Free (RestrB t fnext)) =
  do
    n <- freshs
    next' <- interpretToS (fnext (Ch n))
    return $ Restr (Ch n) (interpretT t) next'

interpretToS (Free (SelectB u l next)) =
      do
        next' <- interpretToS next
        return $ Select u l next'
interpretToS (Free (BranchB u ps)) =
      do
        ps' <- mapM interpretToS ps
        return $ Branch u ps'
interpretToS (Pure ()) = return $ End

runFreshM :: FreshM a -> a
runFreshM m = evalState m initUnique

runFreshMs :: FreshMs a -> a
runFreshMs m = evalState m initUniques
