--  Copyright (c) 2019. Alen Arslanagic, Jorge A. Perez and Erik Voogd
--  and University of Groningen.
--  All rights reserved.
--
--  Redistribution and use in source and binary forms, with or without
--  modification, are permitted provided that the following conditions are met:
--
--  * Redistributions of source code must retain the above copyright notice,
--    this list of conditions and the following disclaimer.
--
--  * Redistributions in binary form must reproduce the above copyright notice,
--    this list of conditions and the following disclaimer in the documentation
--    and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,  THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES;  LOSS OF USE, DATA, OR PROFITS; # OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON  ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF  THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.


{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE GADTs #-}

module Misty.DecompositionTypes where

import Misty.Types
import Misty.Channel

import qualified Data.List as List
import qualified Data.Map as Map
import qualified Data.Set as Set

import Data.Char

--------------------------------------------------------------------------------
-- TYPES DECOMPOSITION
--------------------------------------------------------------------------------
typeDecomp :: TSession -> [MinM]
typeDecomp TBool = [MinBool]
typeDecomp TEnd = [MinEnd]
typeDecomp (TSend v t) = case t of
                            TEnd -> [MinSend [typeDecompAbs v]]
                            _ -> (MinSend [typeDecompAbs v]):(typeDecomp t)
typeDecomp (TRecv v t) = case t of
                            TEnd -> [MinRecv [typeDecompAbs v]]
                            _ -> (MinSend [typeDecompAbs v]):(typeDecomp t)
typeDecomp (TMu t st) = if checkMu st then
                          [MinMu (typeDecomp st!!0)]
                          else
                            typeDecompMu st
typeDecomp (TMuVar t) = [MinMuVar t] -- CHECK THIS
typeDecomp _ = []

typeDecompAbs :: TAbs -> MinU
typeDecompAbs (TAbs t) = MinULin (typeDecomp t)
typeDecompAbs (TAbsP ts) = MinULin (concat $ map typeDecomp ts)

-- this is R
typeDecompMu :: TSession -> [MinM]
typeDecompMu (TMu t st)   = typeDecompMu st
typeDecompMu (TEnd)       = []
typeDecompMu (TSend u t)  = (MinMu $ MinSend [typeDecompAbs u]):(typeDecompMu t)
typeDecompMu (TRecv u t)  = (MinMu $ MinRecv [typeDecompAbs u]):(typeDecompMu t)
typeDecompMu (TMuVar t)   = []
typeDecompMu TBool        = [MinBool]

-- this is R*
typeDecompMuStar :: TSession -> [MinM]
typeDecompMuStar (TMu t st)     = typeDecompMu st
typeDecompMuStar (TSend u st)   = typeDecompMuStar st
typeDecompMuStar (TRecv u st)   = typeDecompMuStar st
typeDecompMuStar x = typeDecompMu x


checkMu :: TSession -> Bool
checkMu ts = checkMuT ts False

checkMuT :: TSession -> Bool -> Bool
checkMuT (TMu t ts) b = if b then
              False
            else
              checkMuT ts True
checkMuT (TSend ta ts) b = checkMuT ts b
checkMuT (TRecv ta ts) b = checkMuT ts b
checkMuT (TMuVar t) b = b
checkMuT (TBool) b = False
checkMuT (TEnd) b = False
checkMuT _ _ = False
