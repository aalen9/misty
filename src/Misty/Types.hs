--  Copyright (c) 2019. Alen Arslanagic, Jorge A. Perez and Erik Voogd
--  and University of Groningen.
--  All rights reserved.
--
--  Redistribution and use in source and binary forms, with or without
--  modification, are permitted provided that the following conditions are met:
--
--  * Redistributions of source code must retain the above copyright notice,
--    this list of conditions and the following disclaimer.
--
--  * Redistributions in binary form must reproduce the above copyright notice,
--    this list of conditions and the following disclaimer in the documentation
--    and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,  THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES;  LOSS OF USE, DATA, OR PROFITS; # OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON  ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF  THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE DeriveFunctor #-}

module Misty.Types where

import Misty.Channel

import Control.Monad.State as State
import Control.Monad.Except as Except
import Control.Monad.Identity as Identity
import Data.Monoid

import qualified Data.List as List
import qualified Data.Map as Map
import qualified Data.Set as Set

--------------------------------------------------------------------------------
-- MONADIC TYPES
--------------------------------------------------------------------------------
data TSession =
    TSend TAbs TSession
  | TRecv TAbs TSession
  | TSl [TSession]
  | TBra [TSession]
  | TBool
  | TEnd
  | TMu MuVar TSession
  | TMuVar MuVar
  deriving (Eq,Ord)

data TAbs =
  TAbs TSession | TAbsP [TSession]
  deriving (Eq, Ord)

data MuVar = MuVar String
  deriving (Eq, Ord)


-- SOURCE TYPES
infixr 6 :!>
infixr 6 :?>

data ST = (:!>) STAbs ST | (:?>) STAbs ST | STEnd | (:+) [ST]
        | (:&) [ST] | STMu MuVar ST | STMuVar MuVar

data STAbs = (:>) ST | (:>>) [ST] | STBool

dualST :: ST -> ST
dualST ((:!>) tabs st) = (:?>) tabs (dualST st)
dualST ((:?>) tabs st) = (:!>) tabs (dualST st)
dualST (STEnd) = STEnd
dualST ((:+) ts) = (:&) (map dualST ts)
dualST ((:&) ts) = (:+) (map dualST ts)
dualST (STMu t st) = STMu t (dualST st)
dualST (STMuVar t) = STMuVar t


dualType :: TSession -> TSession
dualType (TSend tabs tq) = TRecv tabs (dualType tq)
dualType (TRecv tabs tq) = TSend tabs (dualType tq)
dualType (TSl ts) = TBra (map dualType ts)
dualType (TBra ts) = TSl (map dualType ts)
dualType TBool = TBool
dualType (TMu t st) = TMu t (dualType st)
dualType (TMuVar t) = TMuVar t
dualType TEnd = TEnd

interpretT :: ST -> TSession
interpretT (tabs :!> t) = TSend (interpretTAbs tabs) (interpretT t)
interpretT (tabs :?> t) = TRecv (interpretTAbs tabs) (interpretT t)
interpretT STEnd = TEnd
interpretT ((:+) ts) = TSl (map interpretT ts)
interpretT ((:&) ts) = TBra (map interpretT ts)
interpretT (STMu tx t) = TMu tx (interpretT t)
interpretT (STMuVar tx) = TMuVar tx

interpretEnv :: [(ChannelN, ST)] -> [(ChannelN, TSession)]
interpretEnv env = map (\(x,y) -> (x, interpretT y)) env

interpretTAbs :: STAbs -> TAbs
interpretTAbs ((:>) t) = TAbs (interpretT t)
interpretTAbs ((:>>) ts) = TAbsP (map interpretT ts)
interpretTAbs STBool = TAbs TEnd

-- substitution map
newtype SubstTS = SubstTS (Map.Map MuVar TSession)

-- substitutable class
class SubstitableTS a where
  applyTS :: SubstTS -> a -> a


instance SubstitableTS TSession where
  applyTS s@(SubstTS s') (TSend tx ts) =
    TSend (applyTS s tx) (applyTS s ts)
  applyTS s@(SubstTS s') (TRecv tx ts) =
    TRecv (applyTS s tx) (applyTS s ts)
  applyTS s@(SubstTS s') (TSl ts) =
    TSl (map (applyTS s) ts)
  applyTS s@(SubstTS s') (TBra ts) =
    TBra (map (applyTS s) ts)
  applyTS s@(SubstTS s') (TMu t ts) =
    TMu t ts
  applyTS s@(SubstTS s') (TMuVar t) =
    case (Map.lookup t s') of
      Just ty -> ty
      Nothing -> TMuVar t
  applyTS s@(SubstTS s') (TBool) = TBool
  applyTS s@(SubstTS s') (TEnd) = TBool


instance SubstitableTS TAbs where
  applyTS s@(SubstTS s') (TAbs ts) = TAbs (applyTS s ts)
  applyTS s@(SubstTS s') (TAbsP ts) = TAbsP (map (\x -> applyTS s x) ts)


-- reduction on session type level
reduceTSession :: TSession -> TSession
reduceTSession (TSend ta tp) = tp
reduceTSession (TRecv ta tp) = tp
reduceTSession TEnd = TEnd
reduceTSession TBool = TBool
reduceTSession (TMu t ts) = reduceTSession (muTcongruence (TMu t ts))
reduceTSession a = a

-- congruence for mu types
muTcongruence :: TSession -> TSession
muTcongruence (TMu t ts) = applyTS s ts
                              where
                                s = SubstTS $ Map.fromList [(t,TMu t ts)]
muTcongruence a = a


--------------------------------------------------------------------------------
-- POLYADIC TYPES
--------------------------------------------------------------------------------
data PTSession =
    PTSend [PTAbs] PTSession
  | PTRecv [PTAbs] PTSession
  | PTSl [PTSession]
  | PTBra [PTSession]
  | PTBool
  | PTEnd
  | PTMu MuVar PTSession
  | PTt MuVar
  deriving (Eq,Ord)

data PTAbs =
  PTAbs [PTSession]
  deriving (Eq,Ord)

--------------------------------------------------------------------------------
-- MINIMAL TYPES
--------------------------------------------------------------------------------
-- minimal types following the paper

data MinU = MinULin [MinM] | MinUSh [MinM]
  deriving (Eq, Ord)

data MinM =
  MinEnd
  | MinSend [MinU]
  | MinRecv [MinU]
  | MinSh [MinU]
  | MinBool
  | MinSel [MinM]
  | MinBra [MinM]
  | MinMu MinM  
  | MinMuVar MuVar
  deriving (Eq, Ord)
