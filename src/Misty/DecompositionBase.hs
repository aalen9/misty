--  Copyright (c) 2019. Alen Arslanagic, Jorge A. Perez and Erik Voogd
--  and University of Groningen.
--  All rights reserved.
--
--  Redistribution and use in source and binary forms, with or without
--  modification, are permitted provided that the following conditions are met:
--
--  * Redistributions of source code must retain the above copyright notice,
--    this list of conditions and the following disclaimer.
--
--  * Redistributions in binary form must reproduce the above copyright notice,
--    this list of conditions and the following disclaimer in the documentation
--    and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,  THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES;  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON  ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF  THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.


{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE GADTs #-}

module Misty.DecompositionBase where

import Misty.Process
import Misty.Types
import Misty.Channel
import Misty.Latex

import Control.Monad.Except as Except

import qualified Data.List as List
import qualified Data.Map as Map
import qualified Data.Set as Set

import Data.Char

-- TYPES RELATED
data DecompError = NoTypeBinding | NoTypeBindingChannel ChannelN
type ExceptDecomp a = Except DecompError a

printDecompError :: DecompError -> IO ()
printDecompError err =
  case err of
      NoTypeBinding -> putStrLn $ "Misty error: No type binding"
      NoTypeBindingChannel a ->
        putStrLn $ "Misty error: no type binding for channel " ++ printChannel a

incrName :: ChannelN -> Proc -> Proc
incrName ui p = let subs = SubstN $ Map.fromList [(ui, succName ui)] in
                    applyn subs p


getType' :: ChannelN -> [(ChannelN, TSession)] -> ExceptDecomp TSession
getType' ui env =  let mt = List.find (\x -> (fst x) == ui) env in
                    case mt of
                        Just (_,t) -> return $ t
                        Nothing -> throwError NoTypeBinding


getType :: ChannelN -> [(ChannelN, TSession)] -> ExceptDecomp TSession
getType ui env = let n = getBindChannelName ui in
                    let mt = List.find (\x -> (fst x) == n) env in
                        case mt of
                            Just (_,t) -> return $ t
                            Nothing -> throwError $ NoTypeBindingChannel n

printExcept :: Show a => ExceptDecomp a -> String
printExcept a = case runExcept a of
                    Right a' -> show a'
                    Left err -> "Error"

decompInit :: Proc -> Proc
decompInit p = let ftn = Set.toList $ ftvn p
                  in
                    let subst = SubstN $ Map.fromList
                                  $ map (\x -> (x, succName (succName x))) ftn
                      in
                        applyn subst p


txsplit :: [(VarN, MinU)] -> Set.Set VarN -> Set.Set VarN -> ([(VarN, MinU)],[(VarN, MinU)])
txsplit ctx xs ys = let l1 = filter (\x -> Set.member (fst x) xs) ctx in
                        let l2 = filter (\x -> Set.member (fst x) ys) ctx in
                        (l1,l2)

txdel :: [(VarN, MinU)] -> VarN -> [(VarN, MinU)]
txdel props x = filter (\y -> (fst y) /= x) props

xsplit :: [VarN] -> Set.Set VarN -> Set.Set VarN -> ([VarN], [VarN])
xsplit xs xsv xsq = let l1 = filter (\x -> Set.member x xsv) xs in
                        let l2 = filter (\x -> Set.member x xsq) xs in
                        (l1,l2)

latexExcept :: ExceptDecomp PProc -> String
latexExcept exp = case runExcept exp of
                    Right proc -> latexSuppresTypes proc
                    Left err ->
                      case err of
                        NoTypeBinding ->
                                "MST Error: No type binding in the environment"

--------------------------------------------------------------------------------
-- ENVIRONMENT REDUCE
--------------------------------------------------------------------------------

reduceEnv' :: [(ChannelN, TSession)] -> ChannelN -> [(ChannelN, TSession)]
reduceEnv' env u = let m = List.find (\(x,y) -> x == u) env in
                    case m of
                      Just (x1,y1) ->
                        [(succName x1,reduceTSession y1)]++(List.delete (x1,y1) env)
                      Nothing -> env


reduceEnv :: [(ChannelN, TSession)] -> ChannelN -> [(ChannelN, TSession)]
reduceEnv env ui =
    let n = getBindChannelName ui in
      let m = List.find (\(x,y) -> x == n) env in
          case m of
            Just (x1,y1) -> [(x1,reduceTSession y1)]++(List.delete (x1,y1) env)
            Nothing -> env

-- PROCESS DEGREE
lengthProc :: Proc -> Int
lengthProc End = 1
lengthProc (SendVar u v p) = lengthProc p + 1
lengthProc (Send u v p) = case v of
                          Abstr x t p' -> (lengthProc p') + (lengthProc p) + 1
                          AbstrP xs ts p' -> (lengthProc p') + (lengthProc p) + 1
lengthProc (Recv u t x p) = lengthProc p + 1
lengthProc (App v u) = (lengthAbs v) + 1
lengthProc (AppP v us) = (lengthAbs v) + 1
lengthProc (AppVar x u) = 1
lengthProc (AppVarP x us) = 1
lengthProc (Comp p) = case p of
                        (p':ps) -> lengthProc(p') + 1 + (lengthProc (Comp ps))
                        [] -> -1
lengthProc (Restr u t p) = lengthProc p
lengthProc (Select u l p) = lengthProc p + 2
lengthProc (Branch u ps) = 1
lengthProc (BinOp op x y xy p) = lengthProc p


lengthAbs :: Abstr -> Int
lengthAbs (Abstr x t p) = lengthProc p
lengthAbs (AbstrP xs ts p) = lengthProc p
