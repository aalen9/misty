--  Copyright (c) 2019. Alen Arslanagic, Jorge A. Perez and Erik Voogd
--  and University of Groningen.
--  All rights reserved.
--
--  Redistribution and use in source and binary forms, with or without
--  modification, are permitted provided that the following conditions are met:
--
--  * Redistributions of source code must retain the above copyright notice,
--    this list of conditions and the following disclaimer.
--
--  * Redistributions in binary form must reproduce the above copyright notice,
--    this list of conditions and the following disclaimer in the documentation
--    and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,  THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES;  LOSS OF USE, DATA, OR PROFITS; # OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON  ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF  THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.


{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE GADTs #-}

module Misty.DecompositionMu where

import Misty.Channel
import Misty.Process
import Misty.Types
import Misty.DecompositionBase
import Misty.DecompositionTypes

import Control.Monad.State as State
import Control.Monad.Except
import Control.Monad.Identity as Identity
import Data.Monoid

import qualified Data.List as List
import qualified Data.Map as Map
import qualified Data.Set as Set

import Data.Char

unfold :: TSession -> TSession
unfold (TMu t ts) = applyTS  s ts
                      where s = SubstTS $ Map.fromList [(t,TMu t ts)]

indexMu :: TSession -> Int
indexMu tm@(TMu t ts) = indexMu' 0 (unfold tm)
indexMu ts = indexMu' 0 ts


indexMu' :: Int -> TSession -> Int
indexMu' l (TMu t ts) = (length (typeDecompMu ts)) - l + 1
indexMu' l (TSend t ts) = indexMu' (l+1) ts
indexMu' l (TRecv t ts) = indexMu' (l+1) ts


getcmu :: ChannelN -> ChannelN
getcmu n =  case n of
                Ch c -> let (k,n') = getkName c in
                            Ch ("c^" ++ "{" ++ n' ++ "}")
                ChCmpl c -> let (k,n') = getkName c in
                            Ch ("c^" ++ "{" ++ "bar" ++ n' ++ "}")


checkRecurBool :: ChannelN -> [(ChannelN, TSession)] -> Bool
checkRecurBool ui env = let mt = runExcept $ getType ui env in
                            case mt of
                                Right t -> checkMu t
                                Left _ -> False

checkRecur :: ChannelN -> [(ChannelN, TSession)] -> ExceptDecomp Bool
checkRecur ui env = let mt = List.find (\x -> (fst x) == ui) env in
                        case mt of
                            Just (_,t) -> return $ checkMu t
                            Nothing -> throwError NoTypeBinding

--------------------------------------------------------------------------------
-- EXTENSION (2) CORE FRAGMENT WITH RECURSIVE TYPES DECOMPOSITION
--------------------------------------------------------------------------------
decompProc :: Proc -> Int -> [(ChannelN, TSession)] -> [(VarN, MinU)] -> ExceptDecomp [PProc]
decompProc (Send ui v q) k env xprops =
    do
        v' <- decompValueMu (incr_v) (k+1) env yprops
        t2 <- decompProcMu incr_q (k+r+1) env' zprops
        let t1 = PRecv (getc k True) xs txs
                    $ PSend ui [SAbstr v']
                        $ PSend (getc (k+r+1) False)
                            (map (\x -> SVar x) zs) PEnd in
            return $ [t1] ++ t2
            where
                (yprops, zprops) = txsplit xprops (ftvv v) (ftvv q)
                (ys, tys) = unzip yprops
                (zs, tzs) = unzip zprops
                (xs, txs) = unzip xprops
                r = lengthAbs v
                subs = SubstN $ Map.fromList [(ui, succName ui)]
                incr_v = applyn subs v
                incr_q = incrName ui q
                env' = reduceEnv env ui


decompProc (SendVar ui y q) k env xprops  =
    do
        t2 <- decompProcMu incr_q (k+1) env' zprops
        return $ [t1] ++ t2
          where
            zprops = txdel xprops y
            (xs, txs) = unzip xprops
            (zs, tzs) = unzip zprops
            incr_q = incrName ui q
            env' = reduceEnv env ui
            t1 = PRecv (getc k True) (xs) (txs)
                  $ PSend ui [SVar y]
                    $ PSend (getc (k+1) False)
                        (map (\x -> SVar x) zs) PEnd

decompProc (Recv ui t y q) k env xprops =
    do
        t2 <- decompProcMu (incrName ui q) (k+1) env' zprops
        return $ [t1] ++ t2
          where
            decompt@(MinULin decomps) = typeDecompAbs t
            (xs, txs) = unzip xprops
            zprops = xprops ++ [(y,decompt)]
            (zs, tzs) = unzip zprops
            env' = reduceEnv env ui
            t1 = PRecv (getc k True) (xs) (txs)
                  $ PRecv ui [y] [decompt]
                    $ PSend (getc (k+1) False) (map (\x -> SVar x) zs) PEnd


decompProc (App v ui) k env xprops =
    case v of
      (Abstr n t p) ->
        do
            v' <- decompValue v (k+1) env xprops
            return $ [PRecv (getc k True) xs txs $ PApp v' us]
                          where
                            (xs,txs) = unzip xprops
                            decompt = typeDecomp t
                            r = length decompt
                            us = utupleto r ui

decompProc (AppVar y ui) k env xprops =
    case runExcept $ getType ui env of
        Right t' -> return $ [PRecv (getc k True) xs txs $ PAppVar y us]
                        where
                            r = length (typeDecomp t')
                            us = utupleto r ui
                            (xs, txs) = unzip xprops
        Left err -> throwError err

decompProc (Comp pl) k env xprops =
  case pl of
      [] -> return $ []
      (p:[]) -> decompProcMu p k env xprops
      (p:ps) -> do
                    t2 <- decompProcMu p (k+1) env xpprops
                    t3 <- decompProcMu (Comp ps) (k+r+1) env xpsprops
                    return $ [t1] ++ t2 ++ t3
                  where
                    (xpprops, xpsprops) = txsplit xprops (ftvv p) (ftvv (Comp ps))
                    r = lengthProc p
                    (xs, txs) = unzip xprops
                    (xp, txp) = unzip xpprops
                    (xps, txps) = unzip xpsprops
                    t1 = PRecv (getc k True) (xs) (txs)
                              $ PSend (getc (k+1) False)
                                      (map (\x -> SVar x) xp)
                                $ PSend (getc (k+r+1) False)
                                        (map (\x -> SVar x) xps) PEnd

decompProc (Restr s t p) k env xprops =
    do
        t1 <- decompProcMu p'' k env' xprops
        return $ [PRestr (zip stuple decomp) (PComp t1)]
            where
            s' = succName (succName s)
            decomp = typeDecomp t
            p' = (substituteName s s' p)
            p'' = (substituteName (compl s) (succName (succName (compl s))) p')
            --env' = [(s',t)]++[(compl s', dualType t)] ++ env
            env' = [(s,t),(compl s, dualType t)] ++ env
            stuple = xtuple s (length decomp)

decompProc End k env xprops =
  return $ [t1]
    where
      (xs,txs) = unzip xprops
      t1 = PRecv (getc k True) xs txs PEnd

decompValue :: Abstr -> Int -> [(ChannelN, TSession)] -> [(VarN, MinU)] -> ExceptDecomp PAbstr
decompValue (Abstr u t p) k env xprops =
    do
        t2 <- decompProcMu p' k env' xprops
        return $ PAbstr us ts $ PComp $ [t1] ++ t2
            where
                (xs,txs) = unzip xprops
                env' = [(u, t)] ++ env
                ts = typeDecomp t
                us = xtuple (Ch "y") (length ts)
                p' = substituteName u (head us) p
                t1 = PSend (getc k False) (map (\x -> SVar x) xs) PEnd

decompValue (AbstrP us ts p) k env xprops =
    do
        t2 <- decompProcMu p' k env' xprops
        return $ PAbstr (concat uss) (concat tss) $ PComp $ [t1] ++ t2
        where
            (xs,txs) = unzip xprops
            us' = map (succName . succName) us
            env' = (zip us ts) ++ env
            tss = map typeDecomp ts
            uss = map (\ts -> xtuple (Ch "y") (length ts)) tss
            subs = SubstN $ Map.fromList $ zip us us'
            p' = applyn subs p
            t1 = PSend (getc k False) (map (\x -> SVar x) xs) PEnd

decompProcMu :: Proc -> Int -> [(ChannelN, TSession)] -> [(VarN, MinU)] -> ExceptDecomp [PProc]
decompProcMu (Send ui v q) k env xprops | checkRecurBool ui env =
            do
                t <- getType ui env
                tyzs <- return $ typeDecompMuStar t
                zs <- return $ xtuple (Ch "z") (length $ tyzs)
                k1 <- return $ indexMu t
                zk <- return $ Ch ("z" ++ show k1)
                v' <- decompValueMu v (k+1) env yprops
                t2 <- decompProcMu (incrName ui q) (k+r+1) env' wprops
                let n = PAbstr zs tyzs
                            $ PSend zk [SAbstr v']
                                $ PSend (getc (k+r+1) False) (map (\x -> SVar x) ws)
                                    $ PRecv (getcmu ui) ["b"] [MinULin tyzs]
                                        $  PAppVar "b" zs in
                     let t1 = PRecv (getc k True) (xs) (txs)
                                $ PSend (compl $ getcmu ui) [SAbstr n]
                                    $ PEnd in
                        return $ [t1] ++ t2
                    where
                        (yprops, wprops) = txsplit xprops (ftvv v) (ftvv q)
                        (ys, tys) = unzip yprops
                        (ws, tws) = unzip wprops
                        (xs, txs) = unzip xprops
                        r = lengthAbs v
                        env' = reduceEnv env ui

decompProcMu (Send ui v q) k env xprops | not $ checkRecurBool ui env =
    decompProc (Send ui v q) k env xprops

decompProcMu (Recv ui ty y q) k env xprops | checkRecurBool ui env =
    case runExcept $ getType ui env of
        Right t -> do
                    t2 <- decompProcMu (incrName ui q) (k+1) env' wprops
                    return $ [t1] ++ t2
                    where
                        decompt@(MinULin decomps) = typeDecompAbs ty
                        (xs, txs) = unzip xprops
                        wprops = xprops ++ [(y,decompt)]
                        (ws, tws) = unzip wprops
                        k1 = indexMu t
                        zk = Ch ("z" ++ show k1)
                        zs = xtuple (Ch "z") (length $ typeDecompMuStar t)
                        tyzs = typeDecompMuStar t
                        env' = reduceEnv env ui
                        n = PAbstr zs tyzs
                                $ PRecv zk [y] [typeDecompAbs ty]
                                    $ PSend (getc (k+1) False) (map (\x -> SVar x) ws)
                                        $ PRecv (getcmu ui) ["b"] [MinULin tyzs]
                                            $ PAppVar "b" zs
                        t1 = PRecv (getc k True) (xs) (txs)
                                $ PSend (compl $ getcmu ui) [SAbstr n]
                                    $ PEnd
        Left err -> throwError err

decompProcMu (Recv ui ty y q) k env xprops | not $ checkRecurBool ui env =
    decompProc (Recv ui ty y q) k env xprops

decompProcMu (SendVar ui y q) k env xprops | checkRecurBool ui env =
    case runExcept $ getType ui env of
        Right t -> do
                    t2 <- decompProcMu (incrName ui q) (k+1) env' wprops
                    return $ [t1] ++ t2
                        where
                            wprops = txdel xprops y
                            (ws, tws) = unzip wprops
                            (xs, txs) = unzip xprops
                            k1 = indexMu t
                            zk = Ch ("z" ++ show k1)
                            zs = utupleto (length $ typeDecompMuStar t) (Ch "z")
                            tyzs = typeDecompMuStar t
                            env' = reduceEnv env ui
                            n = PAbstr zs tyzs
                                    $ PSend zk [SVar y]
                                        $ PSend (getc (k+1) False) (map (\x->SVar x) ws)
                                        $ PRecv (getcmu ui) ["b"] [MinULin tyzs]
                                            $  PAppVar "b" zs
                            t1 = PRecv (getc k True) (xs) (txs)
                                    $ PSend (compl $ getcmu ui) [SAbstr n]
                                        $ PEnd
        Left err -> throwError err

decompProcMu (SendVar ui y q) k env xprops | checkRecurBool ui env =
    decompProc (SendVar ui y q) k env xprops

decompProcMu (AppP v us) k env xprops =
    do
        v' <- decompValueMu v (k+1) env xprops
        return $ [PRecv (getc k True) xs txs
                    $ produce_q rts rus 0 (PApp v' (zs ++ ms))]
            where
                mts = map (\ui ->
                            List.find (\x -> (fst x) == getBindChannelName ui) env) us
                ts = map (\mt -> case mt of
                                    Just (u',t) -> t
                                    Nothing -> TEnd) mts
                r = case List.findIndex (\x -> not $ checkMu x) ts of
                        Just r' -> r'
                        Nothing -> length ts
                (rts, gts) = splitAt r ts
                (rus, gus) = splitAt r us
                (xs,txs) = unzip xprops
                zs = produce_msx_recurz rts
                ms = produce_ms gts gus


decompProcMu (AppVarP y us) k env xprops  =
    return $ [PRecv (getc k True) xs txs $ produce_q rts rus 0 q]
    where
        mts = map (\ui ->
                    List.find (\x -> (fst x) == getBindChannelName ui) env) us
        ts = map (\mt -> case mt of
                            Just (u',t) -> t
                            Nothing -> TEnd) mts
        r = case List.findIndex (\x -> not $ checkMu x) ts of
                Just r' -> r'
                Nothing -> length ts
        (rts, gts) = splitAt r ts
        (rus, gus) = splitAt r us
        (xs,txs) = unzip xprops
        ms = produce_ms gts gus
        zs = produce_msx_recurz rts
        q = PAppVar y (zs ++ ms)

decompProcMu (Restr s t p) k env xprops =
    if (not $ checkMu t) then
        decompProc (Restr s t p) k env xprops
        else
          do
            t3 <- decompProcMu p' k env xprops
            return
                $ [PRestr (zip ss ts)
                  $ PRestr [(cs,typcs)] $ PComp $ [t1,t2] ++ t3]
                  where
                      ts = typeDecompMu t
                      ss = xtuple s (length ts)
                      cs = getcmu s
                      typb = MinULin ts
                      typcs = MinSh [typb]
                      subs = SubstN $ Map.fromList
                                [(s, succName s), (compl s, succName (compl s))]
                      t1 = PRecv cs ["b"] [typb] $ PAppVar "b" ss
                      t2 = PRecv (compl cs) ["b"] [typb]
                            $ PAppVar "b" (map compl ss)
                      p' = applyn subs p

decompProcMu (Comp pl) k env xprops =
    case pl of
        [] -> return $ []
        (p:[]) -> decompProcMu p k env xprops
        (p:ps) -> do
                    t2 <- decompProcMu p (k+1) env xpprops
                    t3 <- decompProcMu (Comp ps) (k+r+1) env xpsprops
                    return $ [t1] ++ t2 ++ t3
                        where
                        (xpprops, xpsprops) = txsplit xprops (ftvv p) (ftvv (Comp ps))
                        r = lengthProc p
                        (xs, txs) = unzip xprops
                        (xp, txp) = unzip xpprops
                        (xps, txps) = unzip xpsprops
                        t1 = PRecv (getc k True) (xs) (txs)
                                    $ PSend (getc (k+1) False)
                                            (map (\x -> SVar x) xp)
                                    $ PSend (getc (k+r+1) False)
                                            (map (\x -> SVar x) xps) PEnd

decompProcMu p k env xprops = decompProc p k env xprops


decompValueMu :: Abstr -> Int -> [(ChannelN, TSession)] -> [(VarN, MinU)] -> ExceptDecomp PAbstr
decompValueMu (Abstr u t p) k env xprops = decompValue (Abstr u t p) k env xprops
decompValueMu (AbstrP ys ts p) k env xprops =
    do
        t3 <- decompProcMu p' k env' xprops
        return $  PAbstr yss tys
                $ PRestr (zip cs (replicate (length cs) MinEnd))
                  $ PComp (muprops ++ [t2] ++ t3)
            where
                r = case List.findIndex (\x -> not $ checkMu x) ts of
                        Just r' -> r'
                        Nothing -> length ts
                (rts, gts) = splitAt r ts
                (rys, gys) = splitAt r ys
                yss_recur = produce_msx_recur rts rys
                yss_g = produce_msx gts gys
                yss = yss_recur ++ yss_g
                (xs,txs) = unzip xprops
                tys_recur = concat $ map typeDecompMuStar rts
                tys_g = concat $ map typeDecomp gts
                tys = tys_recur ++ tys_g
                cs = utupleto (lengthProc p) (getc k True)
                muprops = produce_muprops rts rys
                t2 = PSend (getc k False) (map (\x -> SVar x) xs) PEnd
                subs = SubstN $ Map.fromList $ map (\x -> (x, succName (succName x))) ys
                p' = applyn subs p
                env' = env ++ (zip ys ts)

produce_muprops :: [TSession] -> [ChannelN] -> [PProc]
produce_muprops ts ys =
    map f (zip ts ys)
        where
            f (t,yi) = PRecv cyi ["b"] [tb] $ PAppVar "b" yis
                                where
                                    yis = xtuple yi (length $ typeDecompMuStar t)
                                    cyi = getcmu yi
                                    tb = MinULin (typeDecomp t)


-- here the first argument should be only recursive types
produce_q :: [TSession] -> [ChannelN] -> Int -> PProc -> PProc
produce_q (si:ts) (ui:us) i q =
    PSend (compl $ getcmu ui) [SAbstr n] PEnd
        where
            zs = utupletosub (length $ typeDecompMuStar si) (show i) (Ch "z")
            tyzs = typeDecompMuStar si
            n = PAbstr zs tyzs $ produce_q ts us (i+1) q
produce_q [] [] i q = q

produce_ms :: [TSession] -> [ChannelN] -> [ChannelN]
produce_ms ts us =
    foldr (\(si,ui) -> \b -> (utupleto (length $ typeDecomp si) ui) ++ b)
            [] (zip ts us)


produce_msx :: [TSession] -> [ChannelN] -> [ChannelN]
produce_msx ts us =
    foldr (\(si,ui) -> \b -> (xtuple ui (length $ typeDecomp si)) ++ b)
          [] (zip ts us)

produce_msx_recur :: [TSession] -> [ChannelN] -> [ChannelN]
produce_msx_recur ts us =
    concat $ map (\(si,ui) -> xtuple ui (length $ typeDecompMuStar si)) (zip ts us)

produce_msx_recurz :: [TSession] -> [ChannelN]
produce_msx_recurz ts =
    let n = length ts in
      let us = map (\x -> Ch ("z" ++ "^{" ++ show x ++ "}")) [0..(n-1)] in
          produce_msx_recur ts us

-- main decomposition function
runDecompMuExcept :: Proc -> [(ChannelN, TSession)] -> ExceptDecomp PProc
runDecompMuExcept p env =
    do
        t2 <- decompProcMu p 1 env []
        return $ PRestr cs' $ PRestr cr' $ PComp $ muprops ++ [t1] ++ t2
            where
                cs = xtuple (Ch "c") (lengthProc p)
                cs' = zip cs (replicate (length cs) MinEnd)
                recur_env = List.filter (\x -> checkMu (snd x)) env
                (recur_names, recur_types) = unzip recur_env
                cr = map getcmu recur_names
                cr' = zip cr (replicate (length cr) MinEnd)
                muprops = produce_muprops recur_types recur_names
                t1 = PSend (getc 1 False) [] PEnd


runDecompMu :: Proc -> [(ChannelN, TSession)] -> PProc
runDecompMu p env = case runExcept $ decompProcMu p 1 env [] of
                    Right pproc -> PComp pproc
                    Left err -> PEnd
