--  Copyright (c) 2019. Alen Arslanagic, Jorge A. Perez and Erik Voogd
--  and University of Groningen.
--  All rights reserved.
--
--  Redistribution and use in source and binary forms, with or without
--  modification, are permitted provided that the following conditions are met:
--
--  * Redistributions of source code must retain the above copyright notice,
--    this list of conditions and the following disclaimer.
--
--  * Redistributions in binary form must reproduce the above copyright notice,
--    this list of conditions and the following disclaimer in the documentation
--    and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,  THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES;  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON  ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF  THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE GADTs #-}

module Misty (module Misty, 
                module Misty.Process, 
                module Misty.Types, 
                module Misty.Channel) where

import Misty.Process
import Misty.Types
import Misty.Semantics
import Misty.DecompositionBase
import Misty.Decomposition as Decomposition
import Misty.DecompositionMu as DecompositionMu
import Misty.Latex
import Misty.Channel

import Control.Monad.Except

import Data.Char

output_dir = "output/"
tex_ext = ".tex"

mistyfp :: (ProcF (), [(ChannelN, ST)], String) -> FilePath -> IO ()
mistyfp (p,env,fn) fp =
    let fp' = if (length fp == 0) then  
            output_dir ++ fn ++ tex_ext
            else 
                fp in 
        case runExcept 
                $ Decomposition.runDecompExcept (runFreshMs (interpretToS p)) 
                    (interpretEnv env) of
            Right decomp ->
                do
                    writeFile fp' content
                    putStrLn $ "Misty Latex written at: " ++ fp'
                    where
                        reduces = reduceFull (runFreshMs (interpretToS p))
                        reduces_decomp = reducePFull decomp
                        content = getLatexContent fn reduces reduces_decomp
            Left err -> printDecompError err

misty :: (ProcF (), [(ChannelN, ST)], String) -> IO ()
misty (p,env,fn) = mistyfp (p,env,fn) fp
                where
                    fp = output_dir ++ fn ++ tex_ext


mistymu :: (ProcF (), [(ChannelN, ST)], String) -> IO ()
mistymu (p,env,fn) = mistymufp (p,env,fn) fp
                        where
                            fp = output_dir ++ fn ++ tex_ext


mistymufp :: (ProcF (), [(ChannelN, ST)], String) -> FilePath -> IO ()
mistymufp (p,env,fn) fp =
    let fp' = if (length fp == 0) then  
                output_dir ++ fn ++ tex_ext
                else 
                    fp in  
        case runExcept 
                $ DecompositionMu.runDecompMuExcept (runFreshMs (interpretToS p)) 
                    (interpretEnv env) of
            Right decomp ->
                    do  
                        writeFile fp' content
                        putStrLn $ "Misty Latex written at: " ++ fp'
                        where
                            reduces = reduceFull (runFreshMs (interpretToS p))
                            reduces_decomp = reducePFull decomp
                            content = getLatexContent fn reduces reduces_decomp
            Left err -> printDecompError err

