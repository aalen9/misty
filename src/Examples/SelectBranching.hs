module Examples.SelectBranching where

import Misty

-- programs
printInt :: ChannelN -> ProcF ()
printInt n = app v n
                where
                    v = abstr typint $ \x -> end

q :: ChannelN -> ProcF ()
q u = do
        branch u [q1,q2]
        where
            vadd = pabstr [typint, typint]
                        $ \xs -> do
                                  res <- add (xs!!0) (xs!!1)
                                  printInt res
            vsub = pabstr [typint, typint]
                        $ \xs -> do
                                    res <- sub (xs!!0) (xs!!1)
                                    printInt res
            q1 = send u vadd
            q2 = send u vsub


r :: ChannelN -> ProcF ()
r u = do
        select u 0
        y <- receive u ty
        pappvar y [Ch "16(Int)", Ch "26(Int)"]

p :: ProcF ()
p = do
        s <- new ts
        par [q s,r (compl s)]

-- types
tx = (:>) STEnd :?> STEnd
ty = (:>) ((:>) STEnd :?> STEnd)
typint = (:>) STEnd :?> STEnd
tabs = (:>) tx :!> STEnd
ts = (:&) [tabs, tabs]

-- enironment
env= [(Ch "16(Int)", typint), (Ch "26(Int)", typint)]

-- packed program : (process, environment, name) 
name = "SelectBranching"
prog :: (ProcF (), [(ChannelN, ST)], String)
prog = (p, env, name)
