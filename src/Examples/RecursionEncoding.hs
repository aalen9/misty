module Examples.RecursionEncoding where

import Misty

-- programs
r :: ProcF ()
r = do
        m <- receive (Ch "a") (STBool)
        sendvar (Ch "a") m
        s <- new ts1
        par [papp v [Ch "a", s], send (compl s) v]
        where
            -- types
            txa = STMu (MuVar "t") $ STBool :?> STBool :!> (STMuVar $ MuVar "t")
            ta = txa
            tv = (:>>) [txa, STMuVar $ MuVar "t"]
            ty1 = STMu (MuVar "t") $ ((:>>) [txa, STMuVar $ MuVar "t"]) :?> STEnd
            tzx = tv
            ts1 = tv :?> STEnd
            -- value
            v = pabstr [txa, ty1]
                    $ \xs -> do
                               zx <- receive (xs!!1) tzx
                               m <- receive  (xs!!0) STBool
                               sendvar (xs!!0) m
                               s <- new ts1
                               par [pappvar zx [xs!!0, s], sendvar (compl s) zx]

q :: ProcF ()
q = do
        send (ChCmpl "a") w
        b <- receive (ChCmpl "a") typa
        sendvar (Ch "q") b
        where
                typa = (:>) STEnd
                typch = typa :?> STEnd
                w = abstr typch $ \a -> end

p :: ProcF ()
p = par [r,q]

-- types
txa = STMu (MuVar "t") $ STBool :?> STBool :!> (STMuVar $ MuVar "t")
ta = txa
tv = (:>>) [txa, STMuVar $ MuVar "t"]
ty1 = STMu (MuVar "t") $ ((:>>) [txa, STMuVar $ MuVar "t"]) :?> STEnd
tzx = tv
ts1 = tv :?> STEnd
ts2 = ts1


p_src :: Proc
p_src = runFreshMs (interpretToS p)


-- environment
ch_a = Ch "a"
typ_a = STMu (MuVar "t") $ STBool :?> STBool :!> (STMuVar $ MuVar "t")
env = [(ch_a, typ_a), (compl ch_a, dualST typ_a)]

-- packed program : (process, environment, name) 
name = "RecursionEncoding"
prog :: (ProcF (), [(ChannelN, ST)], String)
prog = (p, env, name)
