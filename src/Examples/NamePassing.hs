
module Examples.NamePassing where

import Misty

-- programs
q :: ChannelN -> ProcF ()
q u = do
        send u v
        y <- receive (ChCmpl "m") typy
        s <- new typs
        par [appvar y s, send (compl s) absb]
        where
          typm = ((:>) ((:>) typbool :?> STEnd)) :!> STEnd
          typx = (:>) typm
          typz = typx :?> STEnd
          typs = typz
          absb = abstr STEnd $ \b -> end
          typy = ((:>) ((:>) typbool :?> STEnd))
          v = abstr typz $ \z -> do
                                    x <- receive z typx
                                    appvar x (Ch "m")
r :: ChannelN -> ProcF ()
r u = do
        y <- receive u typy
        s <- new typs
        par [appvar y s, send (compl s) w]
        where
          typx' = (:>) typbool
          typz = typx' :?> STEnd
          typw' = (:>) typz
          typx = typw' :!> STEnd
          typm = ((:>) ((:>) typbool :?> STEnd)) :!> STEnd  -- = typx
          typw = (:>) typx
          typs = typw :?> STEnd
          typy = (:>) typs
          w' = abstr typz $ \z -> do
                                    x <- receive z typx'
                                    appvar x (Ch "true")
          w = abstr typx $ \x -> send x w'

p :: ProcF ()
p = do
      u <- new typu
      par [q u, r (compl u)]

-- types
typthunk = (:>) STEnd
typbool = typthunk :!> STEnd  -- it must be of length 1
typm = ((:>) ((:>) typbool :?> STEnd)) :!> STEnd
tx = (:>) typm
tyz = tx :?> STEnd
typv = (:>) tyz
typu = typv :!> STEnd

env = [(Ch "true", typbool), (Ch "m", typm)]

-- packed program : (process, environment, name)
name = "NamePassing"
prog :: (ProcF (), [(ChannelN, ST)], String)
prog = (p, env, name)
