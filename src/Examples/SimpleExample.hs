module Examples.SimpleExample where

import Misty

p :: ChannelN -> ProcF ()
p u = do
        x <- receive u typx
        sendvar u x

-- channel
chu = Ch "u"

-- types
typx = typthunk
typu = typx :?> typx :!> STEnd
typthunk = (:>) STEnd
typbool = typthunk :!> STEnd

-- environment
env = [(Ch "u", typu)]

-- packed program : (process, environment, name)
program = p chu
name = "SimpleExample"
prog = (program, env, name)
