# Misty
**Misty** (**Mi**nimal **S**ession **Ty**pes) tool demonstrates ***minimal session types decomposition*** techniques. It incorporates *a toy programming language*  based on **HO**, a higher-order process calculus with *session types*. **Misty** language syntax closely follows **Cloud Haskell**. This language allows to implement concurrent programs which  exchange messages that may contain other programs.
Programs are typed with standard session types, i.e. session types with sequencing in protocols.
**Misty** demonstates how these programs can be decomposed to programs typable under *minimal session types*, session types without sequencing.

The purpose of **Misty** is to illustrate the mechanisms of the ***minimal session types decomposition***. To this end, **Misty** produces a LaTeX code that incorporates:
1. HO representation of the input **Misty** program with *standard session types*;
2. The reduction chain of the HO process given in (1);
3. The decomposition of the HO process obtained in (1) into an HO process with minimal session;
4. The reduction chain of the decomposed HO process obtained in (3).


**Misty** is implemented as an embedded DSL in Haskell.


# A small example
As a simple example, consider the simple program written in **Misty**:
```haskell
p :: ChannelN -> ProcF ()
p u = do
        x <- receive u typx
        sendvar u x
-- channel
chu = Ch "u"

-- types
typx = typthunk
typu = typx :?> typx :!> STEnd
typthunk = (:>) STEnd
typbool = typthunk :!> STEnd

-- main program
program = p chu
name = "SimpleProgram"
env = [(Ch "u", typu)]
prog = (program, env, name)
```
where `p` is a process that first receives something of type x (`typx`), for the
purpose of this simple example `typx` is the type of the thunk `End -> <>`, along the given channel and sends it back. We define a channel `chu` with name "u" and its session type `typu`. We get `program` by applying `p` to channel `u`. Since channel "u" is free in this program, we construct the typing environment `env` containing a type binding for "u". Finally, we pack a **Misty** program with its environment and name as a tuple `prog`.

A **Misty** process is constructed using Haskell’s `do` notation facilities and a closed **Misty** process has the  type `ProcF ()`.

# Prerequisites
1. Make sure to have a recent version of [Stack](https://docs.haskellstack.org/en/stable/README/) installed (tested with Stack 1.9.3);
2. Make sure to have some version of LaTex engine installed.

# Installation
Usual [Stack](https://docs.haskellstack.org/en/stable/README/) build:
1. Setup using Stack command `stack setup`.
2. Build it with `stack build`.

This will generate **Misty** executable.

# Usage
Users can run predefined examples (in `../examples`) or
write **Misty** programs .


## Running examples
For running **Misty** examples run the executable with `stack exec misty`. Then enter 0-3 to run a corresponding example - 0: SimpleExample, 1: NamePassing, 2: SelectBranching, 3: RecursionEncoding. Next, provide a custom file path (with a custom file name) where LaTeX output should be saved
(for example `user/output/Example1.tex`). If 
file path is not provided, 
**Misty** will output a LaTeX file in the default `../output` folder
with default name (given in a packed program)

## Writing and running Misty programs
In order to write a Misty program these steps are necessary:
1. Import the top-level Misty module `import Misty`;
2. Write a program (with type `Proc F()`);
3. Pack a program, its typing environment (with type `[(ChannelN, ST)]`) and its name `String` as a tuple (with type `(ProcF (), [(ChannelN, ST)], String)`).

where a *typing environment* should consists of type bindings for free channel names of the program. Concretely, typing environment is represented as a list of tuples of the form `(ChannelN, ST)`.
For example `[(Ch "a", (:>) STEnd :?> STEnd)]` encodes **HO** typing environment `a : ?(End-><>);End`.

For example, to run **Misty** program `prog` that is implemented in *UserProgram.hs* follow these steps:

1. Run `stack ghci`;
2. Load a user-defined **Misty** program with `:load UserProgram`;
3. Run `misty prog` to generate a LaTex code (where `prog` should be a packed program as described above) in the default output folder. `mistyfp prog fp` can be used to write a LaTeX code to specified file path `fp`.  If program `prog` contains *recursive session types* channels, run it with `mistymu prog` (or `mistymufp` to write to a specified path).
